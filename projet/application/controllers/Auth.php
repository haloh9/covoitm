<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends CI_Controller {

function __construct(){
 parent::__construct();
 $this->load->helper('cookie');
 $this->load->helper('form');
 $this->load->helper('url');
 $this->load->helper('string');
 $this->load->library('form_validation');
 $this->load->library('email');

 if(is_logged_in())
   redirect('', 'refresh');
}

function index(){
 if($this->input->cookie('remembered_user')){
   $this->load->view('templates/header');
   $this->load->view('login/remember_login');
   $this->load->view('templates/footer');
 } else {
   $this->load->view('templates/header');
   $this->load->view('login/index');
   $this->load->view('templates/footer');
 }
}

/**
** For Login
**/
function login(){
 $this->load->model('User');
 if(get_cookie('remembered_user') == NULL)
  $this->form_validation->set_rules('email', 'email', 'trim|required|valid_email');

 $this->form_validation->set_rules('password', 'password', 'trim|required|xss_clean|callback_verify_user');
 if($this->form_validation->run() == FALSE) { // Si authentification échouer
   if(get_cookie('remembered_user')){
     $this->load->view('templates/header');
     $this->load->view('login/remember_login');
     $this->load->view('templates/footer');
   } else {
     $this->load->view('templates/header');
     $this->load->view('login/index');
     $this->load->view('templates/footer');
   }
 }
 else
   redirect('');
}

function verify_user($password){
  if(get_cookie('remembered_user') != NULL){
    $email = get_cookie('remembered_user');
    $remember_me = NULL;
  } else{
    $email = $this->input->post('email');
    $remember_me = $this->input->post('remember_me');
  }
  $result = $this->User->login($email, $password);
  if($result != false){
    $sess_array = array(
      'id' => $result[0]->user_id,
      'firstname' => $result[0]->user_first_name
    );
    // Si le username et mdp sont correct on check remember_me.
    if($remember_me == 'checked'){
      $remembered_user = array(
      'name'   => 'remembered_user',
      'value'  => $email,
      'expire' => 86500,
      'secure' => false,
      'path'   => '/'
      );
      $this->input->set_cookie($remembered_user);
    }
    $this->session->set_userdata('logged_in', $sess_array);
    return TRUE;
  } else {
      // pour la page login normal
      $this->session->set_flashdata('error_login_message', '<div class="notification is-danger">Adresse email ou mot de passe incorrect.</div>');
      // pour la page remember login
      $this->session->set_flashdata('error_loginCookie_message', '<div class="notification is-danger">Mot de passe incorrect.</div>');
      return false;
  }
}


/**
** For Remember me
**/

function forget_remembered_user(){
 delete_cookie('remembered_user');
 redirect('auth');
}

function verify_recaptcha($recaptcha){
 $response = $this->recaptcha->verifyResponse($recaptcha);
 if (isset($response['success']) and $response['success'] === true)
     return true;
 else {
   $this->form_validation->set_message('verify_recaptcha', 'reCAPTCHA invlaide, essayer encore une fois');
   return false;
 }
}

function reset_password(){
 if($_POST == null){
   $this->load->view('templates/header');
   $this->load->view('reset_password/index');
   $this->load->view('templates/footer');
   return;
 }
 $user_email = $this->input->post('email');
 $this->load->model('User');
 $this->form_validation->set_rules('email', 'email', 'trim|required|valid_email');
 $this->form_validation->set_rules('g-recaptcha-response','Captcha','callback_verify_recaptcha');

 if ($this->form_validation->run() == FALSE){
   $this->load->view('templates/header');
   $this->load->view('reset_password/index');
   $this->load->view('templates/footer');
 } else {
   $user_email = $this->input->post('email');
   $result = $this->User->if_email_exist($user_email);
   if($result){
     $new_password= random_string('alnum', 16);
     $this->User->update_password($result->user_id, $new_password);
     // Envoie d'un mail avec le nouveau mot de passe
     $this->email->from('cantreply@covoitm.com', 'CovoitM');
     $this->email->to($result->user_email);
     $this->email->subject('Password reset');
     $this->email->message('You have requested the new password, :'. $new_password);
     $this->email->send();
     // Affiache page de confirmation
     $this->load->view('templates/header');
     $this->load->view('reset_password/confirmation', array('email' =>  $result->user_email));
     $this->load->view('templates/footer');
   } else {
     $this->session->set_flashdata('reset_pass_msg', '<div class="notification is-danger">Adresse e-mail inexistante.</div>');
     redirect('auth/reset_password');

   }
 }
}


}
?>
