<div class="notification">
  <button class="delete" onclick="hide_modal_notif();"></button>
  <?php if($new_notif):?>
    <h2 class="title"><?php echo count($new_notif); ?> nouvelles notifications</h2>
    <?php foreach ($new_notif as $notif): ?>
    <a href="<?php echo base_url($notif['notification_action']);?>"><article class="media">
      <div class="media-left">
        <span class="icon is-medium">
          <i class="fa fa-bell"></i>
        </span>
      </div>
      <div class="media-content"><?php echo $notif['notification_titre']?><br><small><?php echo $notif['notification_created_at']?></small></div>
    </article></a><br>
    <?php endforeach; ?>
  <?php else:?>
    <h2 class="title">Aucune nouvelle notification</h2>
  <?php endif;?>
  <center><a href="<?php echo base_url('dashboard/mes_notifications')?>" class="button is-primary is-medium">Voir toutes les notifications</a></center>

</div>
