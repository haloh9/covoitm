function changeTab(tab){
  switch (tab) {
    case 'i':
      $( "#contenu_profil_info" ).css( "display", "block" );
      $( "#contenu_profil_avis" ).css( "display", "none" );
      $( "#contenu_profil_trajets" ).css( "display", "none" );
      $( "#contenu_profil_contact" ).css( "display", "none" );
      $( "#tab_profil_info" ).addClass("is-active");
      $( "#tab_profil_avis" ).removeClass("is-active");
      $( "#tab_profil_trajets" ).removeClass("is-active");
      $( "#tab_profil_contact" ).removeClass("is-active");
      break;
    case 'a':
      $( "#contenu_profil_info" ).css( "display", "none" );
      $( "#contenu_profil_avis" ).css( "display", "block" );
      $( "#contenu_profil_trajets" ).css( "display", "none" );
      $( "#contenucontenu_profil_contact" ).css( "display", "none" );
      $( "#tab_profil_info" ).removeClass("is-active");
      $( "#tab_profil_avis" ).addClass("is-active");
      $( "#tab_profil_trajets" ).removeClass("is-active");
      $( "#tab_profil_contact" ).removeClass("is-active");
      break;
    case 't':
      $( "#contenu_profil_info" ).css( "display", "none" );
      $( "#contenu_profil_avis" ).css( "display", "none" );
      $( "#contenu_profil_trajets" ).css( "display", "block" );
      $( "#contenu_profil_contact" ).css( "display", "none" );
      $( "#tab_profil_info" ).removeClass("is-active");
      $( "#tab_profil_avis" ).removeClass("is-active");
      $( "#tab_profil_trajets" ).addClass("is-active");
      $( "#tab_profil_contact" ).removeClass("is-active");
      break;
    case 'c':
      $( "#contenu_profil_info" ).css( "display", "none" );
      $( "#contenu_profil_avis" ).css( "display", "none" );
      $( "#contenu_profil_trajets" ).css( "display", "none" );
      $( "#contenu_profil_contact" ).css( "display", "block" );
      $( "#tab_profil_info" ).removeClass("is-active");
      $( "#tab_profil_avis" ).removeClass("is-active");
      $( "#tab_profil_trajets" ).removeClass("is-active");
      $( "#tab_profil_contact" ).addClass("is-active");
      break;
  }
}
