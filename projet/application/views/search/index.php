<section class="section half-alpha">
  <div class="container">
    <div class="heading">
      <h1 class="title">Rechercher un trajet</h1>
      <h2 class="subtitle">
      </h2>
    </div>
  </div>
</section>
<br>
<div class="columns">
  <div class="column is-1"></div>
  <div class="column auto">
    <div class="box half-alpha">
    <div class="container">
      <form method="get" action="search/results">
        <label class="label">Départ</label>
        <p class="control">
          <input class="input is-primary" type="text" name="from" placeholder="Quartier, rue, avenue, ...">
        </p>

        <br>

        <label class="label">Arrivée</label>
        <p class="control">
          <input class="input is-primary" type="text" name="to" placeholder="Quartier, rue, avenue, ...">
        </p>
        <figure class="image google-logo">
          <img src="<?=base_url('assets/google/powered_by_google_on_white.png')?>" alt="Powered by Google">
        </figure>
        <br>

        <p class="control">
          <center><button type="submit" class="button is-primary is-large">Recherche</button></center>
        </p>
      </form>
    </div>
  </div>
  </div>
  <div class="column is-1"></div>
</div>
</div>
<script type="text/javascript" src="<?php echo base_url('/assets/js/search/search.js');?>"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDUb9C0NnsLI7DWPHFj8QmKnEK6VXIHZjg&libraries=places&callback=initAutocomplete" async defer></script>
