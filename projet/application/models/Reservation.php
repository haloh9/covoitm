<?php
  class Reservation extends CI_Model {
    public function __construct() {
      $this->load->database();
    }

    public function getReservationsByTrajet($trajet_id) {
      $query = $this->db->get_where('reserve', array('trajet_id' => $trajet_id));
      return $query->result_array();
    }

    public function getReservationsByUser($user_id) {
      $query = $this->db->get_where('reserve', array('user_id' => $user_id));
      return $query->result_array();
    }

    public function setReservation($slots, $user_id, $trajet_id) {
      $reservation = array(
        'reserve_date' => date('YmdHis'),
        'reserve_slots' => $slots,
        'user_id' => $user_id,
        'trajet_id' => $trajet_id
      );
      $query = $this->db->insert('reserve', $reservation);
    }
  }
