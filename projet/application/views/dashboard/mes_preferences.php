<div class="column is-7">
  <div class="notification is-warning">
      <h1 class="title">
        Mes préferences
      </h1>
      <?php echo $this->session->flashdata('mes_preferences_msg'); ?>
      <div class="notification is-light">
        <div class="columns">
          <div class="column is-2">
            <span class="icon is-large">
              <img src="<?=base_url("assets/img/smoking.svg");?>" aria-hidden="true">
            </span>
          </div>
          <div class="column is-1" style="padding-top: 30px;">
            <a onclick="changeCigPreference('true')" >
              <span id="cig_true" class="icon is-medium">
                <i class="fa fa-check  is-success" aria-hidden="true"></i>
              </span>
            </a>
          </div>
          <div class="column is-1" style="padding-top: 30px;">
            <a onclick="changeCigPreference('false')">
              <span id="cig_false" class="icon is-medium">
                <i class="fa fa-times" aria-hidden="true"></i>
              </span>
            </a>
          </div>
          <div class="column is-1" style="padding-top: 30px;">
            <a onclick="changeCigPreference('neute')" >
              <span  class="icon is-medium">
                <i id="cig_neute" class="fa fa-minus" aria-hidden="true" ></i>
              </span>
            </a>
          </div>
        </div>
        <br/>
        <div class="columns">
          <div class="column is-2">
            <span class="icon is-large">
              <i class="fa fa-paw" aria-hidden="true"></i>
            </span>
          </div>
          <div class="column is-1" style="padding-top: 30px;">
            <a onclick="changePetsPreference('true')" >
              <span id="pets_true" class="icon is-medium">
                <i class="fa fa-check" aria-hidden="true"></i>
              </span>
            </a>
          </div>
          <div class="column is-1" style="padding-top: 30px;">
            <a onclick="changePetsPreference('false')">
              <span id="pets_false" class="icon is-medium">
                <i class="fa fa-times" aria-hidden="true"></i>
              </span>
            </a>
          </div>
          <div class="column is-1" style="padding-top: 30px;">
            <a onclick="changePetsPreference('neute')" >
              <span id="pets_neute" class="icon is-medium">
                <i class="fa fa-minus" aria-hidden="true"></i>
              </span>
            </a>
          </div>
        </div>
        <br/>
        <div class="columns">
          <div class="column is-2">
            <span class="icon is-large">
              <i class="fa fa-music" aria-hidden="true"></i>
            </span>
          </div>
          <div class="column is-1" style="padding-top: 30px;">
            <a onclick="changeMscPreference('true')" >
              <span id="msc_true" class="icon is-medium">
                <i class="fa fa-check" aria-hidden="true"></i>
              </span>
            </a>
          </div>
          <div class="column is-1" style="padding-top: 30px;">
            <a onclick="changeMscPreference('false')">
              <span id="msc_false" class="icon is-medium">
                <i class="fa fa-times" aria-hidden="true"></i>
              </span>
            </a>
          </div>
          <div class="column is-1" style="padding-top: 30px;">
            <a onclick="changeMscPreference('neute')" >
              <span id="msc_neute" class="icon is-medium">
                <i class="fa fa-minus" aria-hidden="true"></i>
              </span>
            </a>
          </div>
        </div>
        <br/>
        <?php echo form_open('dashboard/mes_preferences'); ?>
          <input type="hidden" value ="<?php echo $preferences['cigarette']?>" name="cigarette" id="cigarette">
          <input type="hidden" value ="<?php echo $preferences['pets']?>" name="pets" id="pets">
          <input type="hidden" value ="<?php echo $preferences['music']?>" name="music" id="music">
          <center>
            <input  class="button is-success is-large" type="submit" value="Modifier" /><br>
          </center>
        <?php echo form_close(); ?>
      </div>
  </div>
</div>
</div>

<script type="text/javascript" src="<?php echo base_url() ?>assets/js/dashboard/mes_preferences.js"></script>
