<?php if ($reviews_received): ?>
  <?php foreach ($reviews_received as $review): ?>
    <article class="media">
      <figure class="media-left">
        <p class="image is-64x64">
          <img class="review_img" src="<?php echo $review['avatar'] ?>">
        </p>
      </figure>
      <div class="media-content ">
        <div class="content">
          <p>
            <strong><a href="<?php echo base_url('profil/user/'.$review['user_id'])?>"><?php echo $review['poster'] ?></a></strong>
            <br>
            <?php echo $review['review_text'] ?>
          </p>
        </div>
        <nav class="level is-mobile">
          <small>Ajouté le : <?php echo $review['review_created_at'] ?> </small>
        </nav>
      </div>
      <div class="media-right">
        <?php for ($i=0; $i<$review['review_stars']; $i++): ?>
          <span class="icon">
            <i class="fa fa-star" style="color:#00d1b2"></i>
          </span>
        <?php endfor;?>
        <?php for ($i=0; $i<(5-$review['review_stars']); $i++): ?>
          <span class="icon">
            <i class="fa fa-star-o" style="color:#00d1b2"></i>
          </span>
        <?php endfor;?>
      </div>
    </article>
  <?php endforeach; ?>
<?php else: ?>
  <div>Aucun avis recu</div>
<?php endif; ?>
