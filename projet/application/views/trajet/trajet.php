<!-- Our JS -->
<script src="<?php echo base_url('assets/js/trajets/trajet.js') ?>" type="text/javascript"></script>

<section class="section">
  <div class="container">
    <div class="heading">
      <h1 class="title">Détails du trajet</h1>
      <h2 class="subtitle"><!--<?php echo var_dump($this->_ci_cached_vars); ?>-->
      </h2>
    </div>
  </div>
</section>
<div class="columns">
  <div class="column is-1"></div>
  <div class="column is-auto">
    <?=$this->session->flashdata('reservation_msg');?>
    <div class="tile is-ancestor">
      <div class="tile is-vertical is-8">
        <div class="tile">
          <div class="tile is-parent is-vertical">
            <article class="tile is-child notification is-primary">
              <p class="title">Départ</p>
              <p class="subtitle"><?php echo $start ?></p>
            </article>
            <article class="tile is-child notification is-danger">
              <p class="title">Arrivée</p>
              <p class="subtitle"><?php echo $destination ?></p>
            </article>
          </div>
          <div class="tile is-parent">
            <article class="tile is-child notification is-info">
              <p class="title">Map</p>
              <p class="subtitle"></p>
              <div id="map">
              </div>
            </article>
          </div>
        </div>
        <div class="tile is-parent">
          <!-- DEBUT PARTIE QUEST REPONSE -->
          <article class="tile is-child notification is-dark">
            <div class="columns">
              <div class="column is-8">
                <p class="title">Poser une question au conducteur </p>
              </div>
              <div class="column">
                <p class="field is-pulled-right">
                    <a class="button is-light" onclick="addquestionr()">
                      <?php if ($logged_user['user_id'] == $creator['user_id']):?>
                        <span>Ajouter une réponse</span>
                      <?php else:?>
                        <span>Ajouter une question</span>
                      <?php endif;?>
                      <span class="icon is-small">
                        <i class="fa fa-plus"></i>
                      </span>
                    </a>
                </p>
              </div>
              <div id="qr_modal" class="modal">
                <div class="modal-background"></div>
                <div class="modal-card">
                  <header class="modal-card-head">
                    <p class="modal-card-title">Question/Reponse : <?php echo $creator['user_first_name'].' '.$creator['user_last_name'] ?></p>
                  </header>
                  <input type="hidden" value ="<?php echo $trajet_id?>" name="trajet_id" id="trajet_id">

                  <div id="qr_content">

                  </div>
                </div>
              </div>
            </div>
            <div id="qr_ajax"></div>
          </article>
          <!-- Fin PARTIE QUEST REPONSE -->

        </div>
      </div>

      <div class="tile is-vertical is-parent">
        <div class="box notification" style="border-radius:0px">
              <center>
                <div class="title">Conducteur</div>
                <figure class="image is-128x128">
                  <img src="<?php echo $creator['user_avatar']?>">
                </figure>
                <br/>
                <p class="subtitle is-6">
                  <?php echo $creator['user_first_name'].' '.$creator['user_last_name'] ?>
                </p>
                <a class="button is-primary is-medium" href="<?php echo base_url('profil/user').'/'.$creator['user_id'] ?>">
                  <span class="icon">
                    <i class="fa fa-arrow-circle-right"></i>
                  </span>
                  <span>Voir son profil</span>
                </a>
            </center>
        </div>
        <article class="tile notification is-light">
          <div class="content">
            <p class="title">Détails</p>
            <p class="subtitle"></p>
            <div class="content">
              <table class="table">
                <tbody>
                  <tr>
                    <th><abbr title="Heure de départ"><span class="icon">
                      <i class="fa fa-clock-o"></i>
                    </span></abbr></th>
                    <td><strong><?=date_to_str($departure).' à '.datetime_to_time($departure);?></strong>
                    </td>
                  </tr>
                  <tr>
                    <th><abbr title="Heure d'arrivée (estimation)"><span class="icon">
                      <i class="fa fa-flag-checkered"></i>
                    </span></abbr></th>
                    <td><strong><?php echo datetime_to_time($eta)?></strong>
                    </td>
                  </tr>
                  <tr>
                    <th><abbr title="Places disponibles"><span class="icon">
                      <i class="fa fa-users"></i>
                    </span></abbr></th>
                    <td><strong><?php echo $free_slots ?></strong>
                    </td>
                  </tr>
                  <tr>
                    <th><abbr title="Distance (estimation)"><span class="icon">
                      <i class="fa fa-arrows-h"></i>
                    </span></abbr></th>
                    <td><strong><?php echo $km ?> km</strong>
                    </td>
                  </tr>
                  <tr>
                    <th><abbr title="Cotisation (estimation)"><span class="icon">
                      <i class="fa fa-euro"></i>
                    </span></abbr></th>
                    <td><strong><?php echo $ept ?>€</strong>
                    </td>
                  </tr>
                  <tr>
                    <th><abbr title="Date de publication"><span class="icon">
                      <i class="fa fa-plus"></i>
                    </span></abbr></th>
                    <td><strong><?php echo date_to_str($added_date); ?></strong>
                    </td>
                  </tr>
                </tbody>
              </table>
              <center>
                <?php if (strcmp($logged_user['user_role'], '1') == 0 && strcmp($logged_user['user_id'], $creator['user_id']) != 0 && strcmp($free_slots, '0') != 0):?>
                  <a href="<?php echo base_url('trajets/reserve/'.$trajet_id.'/'.$creator['user_id']) ?>" class="button is-success is-outlined is-large">Réserver</a>
                <?php elseif (strcmp($logged_user['user_id'], $creator['user_id']) == 0):?>
                  <a href="#" class="button is-success is-outlined is-large" disabled>Réserver</a>
                  <div class="help">Vous ne pouvez pas réserver ce trajet car vous en êtes le propriétaire.</div>
                <?php elseif (strcmp($logged_user['user_role'], '0') == 0):?>
                  <a href="#" class="button is-success is-outlined is-large" disabled>Réserver</a>
                  <div class="help">Vous ne pouvez pas réserver ce trajet car vous possédez un compte <strong>conducteur</strong>.<br><a href="<?=base_url('dashboard/profil')?>">Changer ceci.</a></div>
                <?php elseif (strcmp($free_slots, '0') == 0):?>
                  <a href="#" class="button is-success is-outlined is-large" disabled>Réserver</a>
                  <div class="help">Vous ne pouvez pas réserver ce trajet car toutes les places on été réservées.</div>
                <?php endif;?>
              </center>
            </div>
          </div>
        </article>
      </div>

    </div>

  </div>
  <div class="column is-1"></div>
</div>

<script>

  function trajet_questions_update(){
    $.ajax({
      type: 'GET',
      data: $data,
      url: '<?php echo base_url("/Questions/get_questions_view/".$trajet_id);?>',
      success: function($data){
        $('#qr_ajax').html($data);
      }
    });

  }
  $('document').ready(function(){
    setInterval('trajet_questions_update();',500);
  });
  function addquestionr(){
    document.getElementById('qr_modal').className = "modal is-active";
    $.ajax({
      type: 'GET',
      data: $data,
      url: '<?php echo base_url("/questions/get_add_form_view");?>',
      success: function($data){
        $('#qr_content').html($data);
      }
    });
  }


</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDUb9C0NnsLI7DWPHFj8QmKnEK6VXIHZjg&callback=initMap" async defer></script>
