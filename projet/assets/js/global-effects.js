$(function() {
  $("a, button").hover(hoverIn, hoverOut);
});

function hoverIn(e) {
  $(this).addClass("is-focused");
}
function hoverOut(e) {
  $(this).removeClass("is-focused");
}
