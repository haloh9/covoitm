<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('trajet');
	}

	public function index() {
		$data['body_class'] = 'search';
		$this->load->view('templates/header', $data);
		$this->load->view('search/index');
		$this->load->view('templates/footer');
	}

	public function results()
	{
		$this->load->model('Preference');
		$this->load->model('User');

		$from = $this->input->get('from');
		$to = $this->input->get('to');

		/** Si l'utilisateur est connecté, on récupére ses préférences
		** Sinon, on définit des préférences neutres.
		**/
		if($logged_user = $this->session->userdata('logged_in'))
			$filtres = $this->Preference->getPreference($logged_user['id']);
		else
			$filtres = array('cigarette' => "neute",'pets' => "neute",'music' => "neute");

		if (strcmp($from, "") != 0 && strcmp($to, "") != 0) {
      $data['result'] = $this->trajet->getTrajetFromTo($from, $to, $filtres);
    } else if (strcmp($from, "") != 0){
      $data['result'] = $this->trajet->getTrajetFrom($from, $filtres);
    } else if (strcmp($to, "") != 0) {
      $data['result'] = $this->trajet->getTrajetTo($to, $filtres);

    } else {
				$this->load->view('templates/header');
				$this->load->view('search/no_results');
				$this->load->view('templates/footer');
				return;
		}

		$this->load->view('templates/header');
		if (count($data['result']) < 1) {
      $this->load->view('search/no_results');
    } else {
      $this->load->view('search/results', $data);
    }
		$this->load->view('templates/footer');
	}
}
