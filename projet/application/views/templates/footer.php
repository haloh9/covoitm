</body>
<link rel="stylesheet" href="<?=base_url('assets/css/footer-distributed.css') ?>">



<footer class="footer-distributed">
  <div class="footer-left">
    <h3 class="subtitle is-3">Licenses</h3>
    <p>
      Maps graphic by <a href="http://www.icons8.com">Icons8</a> from <a href="http://www.flaticon.com/">Flaticon</a> is licensed under <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0">CC BY 3.0</a>. Made with <a href="http://logomakr.com" title="Logo Maker">Logo Maker</a><br>
      Icons made by <a href="http://www.flaticon.com/authors/prosymbols" title="Prosymbols">Prosymbols</a> from <a href="http://www.flaticon.com" title="Flaticon">Flaticon</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a>
    </p>
  </div>

  <div class="footer-center">
    <div>
      <i class="fa fa-map-marker"></i>
      <p><span>3 Place Victor Hugo,</span> 13003 Marseille</p>
    </div>
    <div>
      <i class="fa fa-github"></i>
      <p><a href="https://bitbucket.org/haloh9/covoitm">Dépot bitbucket</a></p>
    </div>
    <div>
      <i class="fa fa-envelope"></i>
      <p><a href="mailto:covoitmpia2017@gmail.com">covoitmpia2017@gmail.com</a></p>
    </div>
  </div>

  <div class="footer-right">
    <p class="footer-company-about">
      <span>A propos du projet</span>
      CovoitM est le fruit d'un projet universitaire réalisé par Soufiane Bennani, Hamza El Meknassi et Sifeddine Hireche et encadré par <strong style="color:#00d1b2;">Yi Ren</strong> dans le cadre du module <strong style="color:#00d1b2;">Projet Informatique Appliqué</strong> de L3 Informatique à <a href="http://www.univ-amu.fr/">Aix-Marseille Université</a>.
    </p>
  </div>

</footer>
</html>
