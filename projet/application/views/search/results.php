<!-- Our JS -->
<script src="<?php echo base_url('assets/js/trajets/trajet_card.js') ?>" type="text/javascript"></script>

<section class="section">
  <div class="container">
    <div class="heading">
      <h1 class="title">Resultats</h1>
      <h2 class="subtitle">
      </h2>
    </div>
  </div>
</section>
<div class="container">
  <div class="columns">
    <div class="column is-1"></div>
    <div class="column auto">
      <?=$this->session->flashdata('delete_msg');?>
      <?=$this->session->flashdata('edit_msg');?>
      <?php foreach ($result as $trajet):
        if ($this->session->userdata('logged_in') != NULL) {
          $this->load->view('trajet/trajet_card',
            array('trajet' => $trajet,
            'creator' => $this->User->getUser($trajet['user_id']),
            'logged_user' => $this->User->getUser($this->session->userdata('logged_in')['id'])
          ));
        } else {
          $this->load->view('trajet/trajet_card',
            array('trajet' => $trajet,
            'creator' => $this->User->getUser($trajet['user_id']),
            'logged_user' => array('user_id' => '-1')
          ));
        }

        ?>
        <!--<div class="card">
          <header class="card-header">
            <p class="card-header-title">
              <?php echo substr($trajet['departure'], 0, 10) ?>
              ((Quotidien? Hebdo?))
            </p>
            <a class="card-header-icon">
              <p>((Pseudo Conducteur))</p>
              <span class="icon">
                <i class="fa fa-star-half-o"></i>
              </span>
              0,4
            </a>
          </header>
          <div class="card-content">
            <div class="content">
              <nav class="level">
                <div class="level-item has-text-centered">
                  <div class="notification">
                    <strong>Départ : </strong><?php echo $trajet['start'] ?>
                    <br>
                    à <?php echo substr($trajet['departure'], 11, 5) ?>
                  </div>
                </div>
                <div class="level-item has-text-centered">
                  <div class="notification">
                    <strong>Arrivée : </strong><?php echo $trajet['destination'] ?>
                    <br>
                    à <?php echo substr($trajet['eta'], 11, 5) ?>
                  </div>
                </div>
              </nav>
              <div class="box">
                <center>
                  <span class="tag is-info"><?php echo $trajet['free_slots'] ?> places disponibles</span>
                  <span class="tag is-info"><?php echo $trajet['km'] ?> km environ</span>
                  <span class="tag is-info">~<?php echo $trajet['ept'] ?> € </span>
                </center>
              </div>
              <small>Ajouté <?php echo $trajet['added_date'] ?></small>
            </div>
          </div>
          <footer class="card-footer">
            <a class="card-footer-item">
              <span class="icon">
                <i class="fa fa-question-circle"></i>
              </span>
              <span>Poser une question</span>
            </a>
            <a class="card-footer-item">
              <span class="icon">
                <i class="fa fa-bookmark-o"></i>
              </span>
              <span>Réserver</span>
            </a>
            <a class="card-footer-item">
              <span class="icon">
                <i class="fa fa-info-circle"></i>
              </span>
              <span>Détail du trajet</span>
            </a>
          </footer>
        </div>-->
        <br>
      <?php endforeach; ?>
    </div>
    <div class="column is-1"></div>
  </div>
</div>
