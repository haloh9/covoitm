<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profil extends CI_Controller {

  function __construct() {
    parent::__construct();
  }

  public function index() {
    redirect();
  }

  public function user($id){
    $this->load->model('User');
    $this->load->model('Review');
    $this->load->model('Preference');

    $logged_user = $this->session->userdata('logged_in');
    $user_data = $this->User->getUser($id);

    $data['user'] = array(
          'id' => $id,
          'first_name' => $user_data['user_first_name'],
          'last_name' => $user_data['user_last_name'],
          'gender' => $user_data['user_gender'],
          'age' => date('Y', time())-$user_data['user_birth_year'],
          'phone' => $user_data['user_phone'],
          'role' => $user_data['user_role'],
          'avatar' => $user_data['user_avatar'],
          'bio' => $user_data['user_bio'],
          'created_at' => $user_data['user_created_at'],
          'last_seen' => $user_data['user_last_seen'],
          'status' => $user_data['user_status']
        	);
    $data['logged_user'] = $logged_user['id'];
    $data['reviews_received'] = $this->Review->getReviewsReceived($id);
    $data['average_stars'] = $this->User->getAverageStars($id);
    $data['user_preference'] = $this->Preference->getPreference($id);
    $data['infos_trajet'] = array(
      'nbrTCreated' => $this->Trajet->getNumberOfTrajetsCreatedBy($id),
      'nbrTReserved' => $this->Trajet->getNumberOfTrajetsReservedBy($id)
    );
    $data['mes_trajets'] = $this->Trajet->getTrajetsCreatedBy($id);

    $this->load->view('templates/header');
    $this->load->view('profil/index', $data);
    $this->load->view('templates/footer');
  }
}
