<script>
    // Ajax post
  $(document).ready(function() {
    $("#submit_question_form").click(function(event) {
      event.preventDefault();
      var ques_text = $("#ques_text").val();
      var trajet_id = $("#trajet_id").val();

      jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url("/questions/post_question");?>",
        dataType: 'json',
        data: {ques_text: ques_text, trajet_id: trajet_id},
        success: function(res) {
          if (res){
            if(res.id_return){
              jQuery("#qr_content").html(res.msg_form+res.but_form);

            } else{
              jQuery("#msg_form").html(res.msg_form);
            }
          }
        }
      });
    });
  });

  function hide_modal_question(){
    document.getElementById('qr_modal').className = "modal";
    var qr_ajax = document.getElementById('qr_ajax');
    qr_ajax.scrollTop = qr_ajax.scrollHeight;
  }

</script>

<section class="modal-card-body">
    <div id="msg_form"></div>
  	<div class="columns">
      <div class="column is-3">
        <div class="subtitile is-4"><strong>Votre question</strong></div>
      </div>
      <div class="column is-9">
        <?php echo form_open(); ?>
        <div class="field">
          <p class="control">
            <textarea class="textarea" id="ques_text" name="ques_text" placeholder="....."></textarea>
          </p>
        </div>
        <input id="submit_question_form" class="button is-success" type="submit" value="Publier"/>
        <a onclick="hide_modal_question()" class="button">Retour</a>
        <?php echo form_close(); ?>
      </div>
    </div>

</section>
