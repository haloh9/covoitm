<section class="section">
  <div class="container">
    <div class="heading">
      <h1 class="title">Verification d'adresse mail</h1>
    </div>
  </div>
</section>
<div class="container">
  <div class="columns">
    <div class="column is-1"></div>
    <div class="column auto">
      <div class="notification is-danger">
        <p>Une erreur s'est produite :<p/>
            le lien est incorrect
            <strong>ou</strong> l'adresse e-mail est déjà vérifier
        <br><br>
        <center><a class="button is-medium" href="<?php echo base_url('')?>">Retour à la page d'acceuil</a></center>
      </div>
      <br>
    </div>
    <div class="column is-1"></div>
  </div>
</div>
