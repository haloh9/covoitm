<?php
  class Preference extends CI_Model {

    public function __construct() {
      $this->load->database();
    }

    public function getPreference($user_id) {
      $query = $this->db->get_where('Preference_set', array('user_id' => $user_id));
      return $query->result_array()[0];
    }

    public function setPreference($pref_data, $user_id) {
      $this->db->where('user_id', $user_id);
      $query = $this->db->update('Preference_set', $pref_data);
    }

    public function initPreference($user_id) {
      $query = $this->db->insert('Preference_set', array(
        'cigarette' => 	'neute',
        'pets' => 'neute',
        'music' => 	'neute',
        'user_id' => $user_id
      ));
    }
}
