$(function() {

});

function resizeCard(e) {
  var contentID = $(e).attr("contentid");
  if ($(e).hasClass("collapsed")) {
    $("div."+contentID).css("display", "block");
    $("footer."+contentID).css("display", "flex");
    $(e).removeClass("collapsed");
    $(e).addClass("expanded");
    $("i.fa-angle-down").addClass("fa-angle-up");
    $("i.fa-angle-down").removeClass("fa-angle-down");
  } else if ($(e).hasClass("expanded")) {
    $("div."+contentID).css("display", "none");
    $("footer."+contentID).css("display", "none");
    $(e).removeClass("expanded");
    $(e).addClass("collapsed");
    $("i.fa-angle-up").addClass("fa-angle-down");
    $("i.fa-angle-up").removeClass("fa-angle-up");
  }
}

function populateDropdowns() {
  year = $('#y').val();
  month = $('#m').val();
  day = $('#d').val();
  hour = $('#h').val();
  $('select[name="date-y"]').children("[value=" + year + "]").attr("selected", "selected");
  $('select[name="date-m"]').children("[value=" + month + "]").attr("selected", "selected");
  $('select[name="date-d"]').children("[value=" + day + "]").attr("selected", "selected");
  $('select[name="date-h"]').children("[value=" + hour + "]").attr("selected", "selected");
  console.log($('select[name="date-y"]').children("[value=" + year + "]").val());
}
