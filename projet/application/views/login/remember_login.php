<section class="section">
  <div class="container">
    <div class="heading">
      <h1 class="title">Connexion</h1>
    </div>
  </div>
</section>
<div class="columns">
  <div class="column is-one-third"></div>
  <div class="column auto">
    <div class="content">
      <div class="box">
           <?php echo form_open('auth/login'); ?>
             <h2 class="title">Bonjour <?php echo get_cookie('remembered_user');?>,</h2>
             <h2 class="subtitle">Veuillez saisir votre mot de passe pour continuer</h2>
             <?php echo $this->session->flashdata('error_loginCookie_message');?>
             <div class="field">
               <div class="control is-expanded has-icon has-icon-left">
               <input class="input is-primary is-medium" type="password" size="20" name="password" placeholder="Mot de passe"/>
                <span class="icon is-medium">
                   <i class="fa fa-key"></i>
                 </span>
               </div>
               <a href="<?php echo base_url('auth/reset_password')?>">Mot de passe oublié</a>
            </div>

             <center>
               <input  class="button is-success is-large" type="submit" value="Se connecter"/><br><br>
               <a class="button" href="<?php echo base_url('auth/forget_remembered_user')?>">Ou bien se connecter avec un autre compte</a>
             </center>
           <?php echo form_close(); ?>
      </div>
    </div>
  </div>
  <div class="column is-one-third"></div>
</div>
