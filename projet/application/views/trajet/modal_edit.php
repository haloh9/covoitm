<?php echo form_open('trajets/update'.'/'.$origin, array('id' => 'editForm')); ?>
  <h2 class="subtitle">
    <span class="help is-danger"><?php echo validation_errors();
    echo $this->session->flashdata('flash_message'); ?></span>
  </h2>
  <input type="text" name="trajet_id" value="<?=$trajet['trajet_id']?>" hidden>
  <div class="field is-horizontal">
    <div class="field-label is-normal">
      <label class="label">Départ</label>
    </div>
    <div class="field-body">
      <p class="control has-icon has-icon-left">
        <input class="input is-primary is-medium" type="text" name="start" value="<?=$trajet['start']?>">
        <span class="icon is-medium">
          <i class="fa fa-user"></i>
        </span>
      </p>
    </div>
  </div>

  <div class="field is-horizontal">
    <div class="field-label is-small">
      <label class="label">Destination</label>
    </div>
    <div class="field-body">
      <p class="control has-icon has-icon-left">
        <input class="input is-primary is-medium" type="text" name="destination" value="<?=$trajet['destination']?>">
        <span class="icon is-medium">
          <i class="fa fa-user"></i>
        </span>
      </p>
    </div>
  </div>

  <div class="field is-horizontal">
    <div class="field-label is-normal">
      <label class="label">Places disponibles</label>
    </div>
    <div class="field-body">
      <p class="control has-icon has-icon-left">
        <input class="input is-primary is-medium" type="text" name="free_slots" value="<?=$trajet['free_slots']?>">
        <span class="icon is-medium">
          <i class="fa fa-user"></i>
        </span>
      </p>
    </div>
  </div>
  <input type="text" id="y" value=<?=datetime_to_date($trajet['departure'], 'y', true)?> hidden>
  <input type="text" id="m" value=<?=datetime_to_date($trajet['departure'], 'm', true)?> hidden>
  <input type="text" id="d" value=<?=datetime_to_date($trajet['departure'], 'd', true)?> hidden>
  <input type="text" id="h" value=<?=datetime_to_time($trajet['departure'], 'h', true)?> hidden>
  <div id="departure-edit"></div>
<!-- Form is closed in modal footer -->
