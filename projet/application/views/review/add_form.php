<script>
  function set_stars_review(star){
    for(i=0; i<=5; i++){
      $('#logo_stars_'+i).css('color', '');
    }
    for(i=0; i<=star; i++){
      $('#logo_stars_'+i).css('color', '#00d1b2');
    }
    $('#review_stars').val(star);
  }

    // Ajax post
  $(document).ready(function() {
    $("#submit_review_form").click(function(event) {
      event.preventDefault();
      var review_stars = $("#review_stars").val();
      var review_text = $("#review_text").val();
      var id_user_profil = $("#id_user_profil").val();
      console.log(id_user_profil);
      console.log(review_stars+' // '+review_text);
      jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url("/reviews/post_review");?>",
        dataType: 'json',
        data: {review_stars: review_stars, review_text: review_text, id_user_profil: id_user_profil},
        success: function(res) {
          if (res){
            if(res.id_return){
              jQuery("#content_form_review").html(res.msg_form+res.but_form);
            } else{
              jQuery("#msg_form").html(res.msg_form);
            }
          }
        }
      });
    });
  });

</script>

<section class="modal-card-body">
    <div id="msg_form"></div>
    <br>
    <div class="columns">
      <div class="column is-3">
        <div class="subtitile is-4">Notation</div>
      </div>
      <div class="column is-9">
        <div class="media-right">
          <?php for ($i=1; $i<6; $i++): ?>
            <span class="icon">
              <i id="logo_stars_<?php echo $i ?>" class="fa fa-star" onclick="set_stars_review('<?php echo $i;?>');" ></i>
            </span>
          <?php endfor;?>
        </div>
      </div>
    </div>

  	<div class="columns">
      <div class="column is-3">
        <div class="subtitile is-4">Avis</div>
      </div>
      <div class="column is-9">
        <?php echo form_open(); ?>
        <div class="field">
          <p class="control">
            <input class="input is-primary" type="hidden" id="review_stars" name="review_stars"/>
            <textarea class="textarea" id="review_text" name="review_text" placeholder="Parlez nous du trajet...."></textarea>
          </p>
        </div>
        <input id="submit_review_form" class="button is-success" type="submit" value="Publier"/>
        <a onclick="hide_modal_review()" class="button">Retour</a>
        <?php echo form_close(); ?>
      </div>
    </div>

</section>
