<?php echo $this->recaptcha->getScriptTag(); ?>
<section class="section">
  <div class="container">
    <div class="heading">
      <h1 class="title">Inscription</h1>
    </div>
  </div>
</section>
<div class="columns">
  <div class="column is-one-third"></div>
  <div class="column auto">
    <div class="content">
      <div class="box">
        <?php echo form_open('register/process'); ?>
          <h2 class="subtitle">Veuillez saisir vos informations<br></h2>

          <div class="field is-horizontal">
            <div class="field-body">
              <div class="field">
                  <span class="select is-medium">
                    <?php
                      $options = array(
                        'civilite'    => 'Civilité',
                        '0'           => 'Homme',
                        '1'           => 'Femme'
                      );
                      echo form_dropdown('gender', $options, set_value('gender'));
                    ?>
                  </span>
                <?php echo form_error('gender', '<p class="help is-danger">', '</p>'); ?>
              </div>
              <div class="field">
                  <span class="select is-medium">
                    <?php
                      $options = array(
                        'role'    => 'Role',
                        '0'           => 'Conducteur',
                        '1'           => 'Voyageur'
                      );
                      echo form_dropdown('role', $options, set_value('role'));
                    ?>
                  </span>
                <?php echo form_error('role', '<p class="help is-danger">', '</p>'); ?>
              </div>
            </div>
          </div>

          <div class="field">
            <div class="control has-icon has-icon-left">
              <input class="input is-primary is-medium" type="text" name="last_name" value="<?php echo set_value('last_name'); ?>" placeholder="Nom de famille">
              <span class="icon is-medium">
                <i class="fa fa-user"></i>
              </span>
            </div>
            <?php echo form_error('last_name', '<p class="help is-danger">', '</p>'); ?>
          </div>

          <div class="field">
            <div class="control is-expanded has-icon has-icon-left">
              <input class="input is-primary is-medium" type="text" name="first_name" value="<?php echo set_value('first_name'); ?>" placeholder="Prénom">
              <span class="icon is-medium">
                <i class="fa fa-user"></i>
              </span>
            </div>
            <?php echo form_error('first_name', '<p class="help is-danger">', '</p>'); ?>
          </div>

          <div class="field">
            <div class="control is-expanded has-icon has-icon-left">
              <input class="input is-primary is-medium" type="email" name="email" value="<?php echo set_value('email'); ?>" placeholder="Adresse e-mail" >
              <span class="icon is-medium">
                <i class="fa fa-envelope"></i>
              </span>
            </div>
            <?php echo form_error('email', '<p class="help is-danger">', '</p>'); ?>
            <span class="help is-danger"><?php echo $this->session->flashdata('error_email_message');?></span>
          </div>

          <div class="field">
            <div class="control is-expanded has-icon has-icon-left">
              <input class="input is-primary is-medium" type="password" name="password" placeholder="Mot de passe">
              <span class="icon is-medium">
                <i class="fa fa-key"></i>
              </span>
            </div>
            <?php echo form_error('password', '<p class="help is-danger">', '</p>'); ?>
          </div>

          <div class="field">
            <div class="control is-expanded has-icon has-icon-left">
              <input class="input is-primary is-medium" type="password" name="confirm_password" placeholder="Confirmation mot de passe">
              <span class="icon is-medium">
                <i class="fa fa-repeat"></i>
              </span>
            </div>
            <?php echo form_error('confirm_password', '<p class="help is-danger">', '</p>'); ?>
          </div>

          <div class="field">
            <div class="control is-expanded has-icon has-icon-left">
            <input class="input is-primary is-medium" type="number" name="birth_year" value="<?php echo set_value('birth_year'); ?>" placeholder="Année de naissance">
              <span class="icon is-medium">
                <i class="fa fa-birthday-cake"></i>
              </span>
            </div>
            <?php echo form_error('birth_year', '<p class="help is-danger">', '</p>'); ?>
          </div>

          <div class="field">
            <div class="control is-expanded has-icon has-icon-left">
              <input class="input is-primary is-medium" type="tel" name="phone" value="<?php echo set_value('phone'); ?>" placeholder="Numéro de téléphone">
              <span class="icon is-medium">
                <i class="fa fa-mobile"></i>
              </span>
            </div>
            <?php echo form_error('phone', '<p class="help is-danger">', '</p>'); ?>
          </div>

          <div class="field">
            <div class="control">
              <label class="checkbox"><?php echo form_checkbox(array("id"=>"term_and_conditions", 'name' => "term_and_conditions", 'value' => "checked")); ?>J'ai lu et j'accepte les termes et conditions du site
              </label>
            </div>
            <span class="help is-danger"><?php echo $this->session->flashdata('error_tec_message'); ?></span>
          </div>

          <div class="field">
            <div class="control ">
              <center><?php echo $this->recaptcha->getWidget(); ?></center>
            </div>
            <span class="help is-danger has-text-centered"><?php echo form_error('g-recaptcha-response'); ?></span>
          </div>

          <center>
            <button type="submit" class="button is-success is-large">S'inscrire</button>
          </center>
        <?php echo form_close(); ?>
      </div>
    </div>
  </div>
  <div class="column is-one-third"></div>
</div>
