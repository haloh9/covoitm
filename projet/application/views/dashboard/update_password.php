<div class="column is-7">
  <div class="notification is-primary">
    <div class="subtitle">
      <h1 class="title">Modifier mon mot de passe</h1>
    </div>
    <?php echo form_open('dashboard/update_password'); ?>
      <?php echo $this->session->flashdata('update_pass_succ_msg'); ?>
      <?php echo $this->session->flashdata('update_pass_err_msg'); ?>
      <div class="field">
        <p class="control has-icon has-icon-left">
          <input class="input is-primary is-medium" type="password" size="20" id="new_password" name="new_password" placeholder="Saisissez votre nouveau mot de passe"/>
          <span class="icon is-medium">
            <i class="fa fa-key"></i>
          </span>
        </p>
        <p class="help is-danger"><?php echo form_error('new_password'); ?></p>
      </div>

      <div class="field">
        <p class="control has-icon has-icon-left">
         <input class="input is-primary is-medium" type="password" size="20" id="confirm_new_password" name="confirm_new_password" placeholder="Confirmez votre nouveau mot de passe"/>
         <span class="icon is-medium">
           <i class="fa fa-check-circle-o"></i>
         </span>
        </p>
        <p class="help is-danger"><?php echo form_error('confirm_new_password'); ?></p>
      </div>

      <div class="field">
        <div class="control">
          <center><button type="submit" class="button is-success is-large">Valider</button></center>
        </div>
      </div>

    <?php echo form_close(); ?>
  </div>
</div>
</div>
