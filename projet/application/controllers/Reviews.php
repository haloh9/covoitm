<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Reviews extends CI_Controller {
	function __construct(){
    parent::__construct();
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->load->model('User');
		$this->load->model('Review');
		if (!is_logged_in())
			redirect('auth/login', 'refresh');
  }

	public function get_profil_view($id){
		$logged_user = $this->session->userdata('logged_in');
		$data['reviews_received'] = $this->Review->getReviewsReceived($id);
		$this->load->view('review/profil', $data);
	}

  public function get_add_form_view(){
    $this->load->view('review/add_form');
  }

	public function post_review(){
    $logged_user = $this->session->userdata('logged_in');
		$user_data = $this->User->getUser($logged_user['id']);

		$this->form_validation->set_rules('review_stars', 'notation', 'required|xss_clean|greater_than[0]|less_than[6]');
    $this->form_validation->set_rules('review_text', 'avis', 'required|xss_clean');

		if($this->form_validation->run() == FALSE) {
      $data = array(
        'id_return' => 0,
        'msg_form' => '<div class="notification is-danger">Erreur : l\'utilisateur n\'a pas été noté ou l\'avis n\'a pas été rédiger.</div>'
      );
      echo json_encode($data);
      return;
		}
		$review_text = $this->input->post('review_text');
		$review_stars = $this->input->post('review_stars');
    $id_user_profil = $this->input->post('id_user_profil');

		$review_data= array(
    	"review_text"=> $review_text,
    	"review_stars"=> $review_stars,
			"review_created_at" => date('Y-m-d H:i:s'),
   	 	"user_reviewed"=> $id_user_profil,
    	'user_id'=> $logged_user['id']
    );

    $this->Review->setReview($review_data);
    $url = base_url('profil/user/'.$id_user_profil);
    $data = array(
      'id_return' => 1,
      'msg_form' => '<div class="notification is-success"><div class="title">L\'avis a été ajouter avec succès.</div>',
      'but_form' => '<center><a class="button is-dark" onclick="hide_modal_review()")?>Fermer</a></center></div>'
    );
    echo json_encode($data);
  }

}
