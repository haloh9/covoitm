<?php echo $this->recaptcha->getScriptTag();?>
<section class="section">
  <div class="container">
    <div class="heading">
      <h1 class="title">Récuperer mon mot de passe</h1>
    </div>
  </div>
</section>
<div class="columns">
  <div class="column is-one-third"></div>
  <div class="column auto">
    <div class="content">
      <div class="box">
           <?php echo form_open('auth/reset_password'); ?>
             <h2 class="subtitle">Veuillez saisir votre adresse <br>e-mail de récuperation</h2>

             <?php echo $this->session->flashdata('reset_pass_msg'); ?>
             <div class="field">
               <div class="control is-expanded has-icon has-icon-left">
                 <input class="input is-primary is-medium" type="email" size="20" name="email" value="<?php echo set_value('email'); ?>" placeholder="Adresse e-mail" required/>
                 <span class="icon is-medium">
                   <i class="fa fa-envelope"></i>
                 </span>
               </div>
               <?php echo form_error('email', '<p class="help is-danger">', '</p>'); ?>
             </div>
             <div class="field">
               <div class="control ">
                 <center><?php echo $this->recaptcha->getWidget(); ?></center>
               </div>
               <span class="help is-danger has-text-centered"><?php echo form_error('g-recaptcha-response'); ?></span>
             </div>
             <center>
               <a class="button is-dark is-large" href="<?php echo base_url('auth')?>">Retour</a>
               <input  class="button is-success is-large" type="submit" value="Envoyer" /><br>
             </center>
           <?php echo form_close(); ?>
      </div>
    </div>
  </div>
  <div class="column is-one-third"></div>
</div>
