<!DOCTYPE html>
<html>
  <head>
    <title>CovoitM</title>
    <!-- Styles -->
    <link rel="stylesheet" href="<?=base_url('assets/css/thirdparty/font-awesome-4.7.0/css/font-awesome.min.css') ?>">
    <link rel="stylesheet" href="<?=base_url('assets/css/thirdparty/bulma-0.4.0/css/bulma.css') ?>">
    <link rel="stylesheet" href="<?=base_url('assets/css/style.css') ?>">
    <!-- JQuery -->
    <script src="<?=base_url('assets/js/thirdparty/jquery-3.2.0.js') ?>"></script>
    <!-- Events JS -->
    <script src="<?=base_url('assets/js/global-effects.js')?>" type="text/javascript"></script>
    <script>
      var $data;

      function notif_update(){
        $.ajax({
          type: 'GET',
          data: $data,
          url: '<?php echo base_url("/notifications/get_nbrof_notif");?>',
          success: function($data){$('#notifications-covoit').html($data);}
        });
      }
      $('document').ready(function(){
        setInterval('notif_update();',2000);
      });
      function show_modal_notif(){
        document.getElementById('all_notifs').className = "modal is-active";
        $.ajax({
          type: 'GET',
          data: $data,
          url: '<?php echo base_url("/notifications/get_new_notif");?>',
          success: function($data){
            $('#text_notif').html($data);
          }
        });
      }
      function hide_modal_notif(){
        document.getElementById('all_notifs').className = "modal";
      }
    </script>
  </head>

  <body class="<?php if (isset($body_class)) echo $body_class; ?>">
    <nav class="nav has-shadow">
      <div class="container">
        <div class="nav-left">
          <a class="nav-item" href="<?php echo base_url() ?>">
            <img src="<?php echo base_url() ?>/assets/logo.png" alt="CovoitM logo" >
          </a>
        </div>
        <span class="nav-toggle">
          <span></span>
          <span></span>
          <span></span>
        </span>
        <div class="nav-right nav-menu">
          <?php if (!is_logged_in()): ?>
          <span class="nav-item">
            <a class="button is-primary" href="<?php echo base_url('auth')?>">Connexion</a>
            <a class="button is-primary" href="<?php echo base_url('register')?>">Inscription</a>
          </span>
          <?php else: ?>
          <a class="nav-item" href="<?php echo base_url('dashboard')?>">
            <figure class="image is-24x24" style="margin-right: 8px;">
              <img src="<?=$this->User->getUser($this->session->userdata('logged_in')['id'])['user_avatar']?>">
            </figure>
            <?=$this->session->userdata('logged_in')['firstname'];?>
          </a>
          <span class="nav-item">
            <a onclick="show_modal_notif();" >
              <span class="icon">
                <i class="fa fa-bell"></i>
              </span>
              <span id="notifications-covoit"></span>
            </a>
          </span>
          <span class="nav-item">
            <a class="button is-primary" href="<?php echo base_url('dashboard/logout')?>">Se déconnecter</a>
          </span>
          <?php endif; ?>

        </div>
      </div>
    </nav>

    <div id="all_notifs" class="modal">
      <div class="modal-background"></div>
      <div id="text_notif" class="modal-content"></div>
    </div>
    <?php if (!is_verified()): ?>
      <div id="nav_no_verify">
        Votre compte n'est pas verifié, cliquez ici pour renvoyer un e-mail de vérification. </div>
    <?php endif; ?>
