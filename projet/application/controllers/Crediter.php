<?php
class Crediter extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->output->set_status_header('404');
        $this->load->view('templates/header');
        $this->load->view('templates/404');//loading in my template
        $this->load->view('templates/footer');
    }

    public function success($amount){
      $logged_user = $this->session->userdata('logged_in');
      $user_data = $this->User->getUser($logged_user['id']);

      $new_balance = $user_data['user_balance'] + $amount;
      $user_data = $this->User->update_wallet($logged_user['id'], $new_balance);

      $data['info'] = array(
  	    'amount' => $amount,
  	    'new_balance' => $new_balance
    	);
      $this->load->view('templates/header');
  		$this->load->view('dashboard/menu');
  		$this->load->view('crediter/success', $data);
  		$this->load->view('templates/footer');
    }

    public function failed(){
      $this->load->view('templates/header');
  		$this->load->view('dashboard/menu');
  		$this->load->view('crediter/failed');
  		$this->load->view('templates/footer');
    }
}
?>
