<section class="section">
  <div class="container">
    <div class="heading">
      <h1 class="title">Confirmation</h1>
    </div>
  </div>
</section>
<div class="container">
  <div class="columns">
    <div class="column is-1"></div>
    <div class="column auto">
      <div class="notification is-success">
        <p>Mot de passe changé avec succés !<p>
        <p>Le nouveau mot de passe vous a été envoyer par mail à l'adresse suivante : <strong><?php echo $email?></strong></p>
        <br>
        <center><a class="button is-medium" href="<?php echo base_url('auth')?>">Se connecter</a></center>
      </div>
      <br>
    </div>
    <div class="column is-1"></div>
  </div>
</div>
