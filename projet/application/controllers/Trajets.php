<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Trajets extends CI_Controller {
	function __construct(){
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Methods: GET, OPTIONS');
		header('Access-Control-Allow-Headers: Content-Type, Content-Length, Accept-Encoding');
    parent::__construct();
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->load->model('Trajet');
		$this->load->model('User');
		$this->load->model('Reservation');
		$this->load->model('Notification');

		if (!is_logged_in())
			redirect('auth/login', 'refresh');
  }

	public function create() {
		$this->load->view('templates/header');
		$this->load->view('trajet/create');
		$this->load->view('templates/footer');
	}

	public function process_reservation() {
		$trajet = $this->Trajet->getTrajetByID($_POST['trajet_id']);
		$creator = $this->User->getUser($_POST['creator_id']);
		$customer = $this->User->getUser($this->session->userdata('logged_in')['id']);

		if ($_POST['reserve_slots']*$trajet['ept'] <= $customer['user_balance']) {
			$this->Reservation->setReservation($_POST['reserve_slots'], $customer['user_id'], $trajet['trajet_id']);
			$new_balance = $customer['user_balance']-($_POST['reserve_slots']*$trajet['ept'])-0.5;
			$this->User->update_wallet($customer['user_id'], $new_balance);
			$new_free_slots = $trajet['free_slots']-$_POST['reserve_slots'];
			$this->Trajet->update($_POST['trajet_id'], array('free_slots' => $new_free_slots));
			$notification_title = $customer['user_first_name'].' '.$customer['user_last_name'].' a réservé '.$_POST['reserve_slots'].' places.';
			$this->Notification->setNotif($creator['user_id'], $notification_title, 'trajets/trajet/'.$trajet['trajet_id']);
			$this->session->set_flashdata('reservation_msg', '<div class="notification is-success">Votre réservation a bien été prise en compte.</div>');
		} else {
			$this->session->set_flashdata('reservation_msg', '<div class="notification is-danger">Une erreur s\'est produite lors de la réservation. Votre compte n\'a pas été débité.</div>');
		}

		redirect('trajets/trajet/'.$_POST['trajet_id']);
	}

	public function process_trajet_quotidien() {
		$date_begin = strtotime($_POST['date-y-start'].'-'.$_POST['date-m-start'].'-'.$_POST['date-d-start']);
		$date_end = strtotime($_POST['date-y-end'].'-'.$_POST['date-m-end'].'-'.$_POST['date-d-end']);
		$date_diff = $date_end - $date_begin;
		$total_days = floor($date_diff / (24*60*60));
		$group = generate_group();
		$next_day = $date_begin; //variable incremented with one day each iteration
		/*var_dump($date_begin);
		var_dump($date_end);
		var_dump($date_diff);
		var_dump($total_days);
		var_dump($group);*/
		for ($i=0; $i<=$total_days; $i++) {
			$user_data = $this->session->userdata('logged_in');
			$new_trajet = array(
				'departure' => date('Ymd', $next_day).$_POST['date-h'].'0000',
				'start' => $_POST['start'],
				'destination' => $_POST['destination'],
				'eta' => date('YmdHis', time()),
				'free_slots' => $_POST['free_slots'],
				'recurrence' => $_POST['recurrence'],
				'km' => 7,
				'ept' => 5,
				'added_date' => date('YmdHis', time()),
				'grouping' => $group,
				'user_id' => $user_data['id']
			);
			//var_dump($new_trajet);
			$this->db->insert('Trajets', $new_trajet); //Use the model here
			$next_day += (24*60*60);
		}
	}

	public function process_trajet_hebdo() {
		$date_begin = strtotime($_POST['date-y-start'].'-'.$_POST['date-m-start'].'-'.$_POST['date-d-start']);
		$date_end = strtotime($_POST['date-y-end'].'-'.$_POST['date-m-end'].'-'.$_POST['date-d-end']);
		$date_diff = $date_end - $date_begin;
		$total_days = floor($date_diff / (24*60*60));
		$group = generate_group();
		$next_day = $date_begin; //variable incremented with one day each iteration
		$days_chosen = days_to_array($_POST);
		/*var_dump($date_begin);
		var_dump($date_end);
		var_dump($date_diff);
		var_dump($total_days);
		var_dump($group);*/
		var_dump($days_chosen);
		for ($i=0; $i<=$total_days; $i++) {
			$user_data = $this->session->userdata('logged_in');
			if (in_array(getdate($next_day)['weekday'], $days_chosen)) {
				$input_name = 'date-h-'.strtolower(getdate($next_day)['weekday']);
				$new_trajet = array(
					'departure' => date('Ymd', $next_day).$_POST[$input_name].'0000',
					'start' => $_POST['start'],
					'destination' => $_POST['destination'],
					'eta' => date('YmdHis', time()),
					'free_slots' => $_POST['free_slots'],
					'recurrence' => $_POST['recurrence'],
					'km' => 7,
					'ept' => 5,
					'added_date' => date('YmdHis', time()),
					'grouping' => $group,
					'user_id' => $user_data['id']
				);
				/*echo '<br/>';
				var_dump($new_trajet);*/
				$this->db->insert('Trajets', $new_trajet); //Use the model here
			}
			$next_day += (24*60*60);
		}
	}

	public function process_trajet_ponctuel() {
		$user_data = $this->session->userdata('logged_in');
		$new_trajet = array(
			'departure' => $_POST['date-y'].$_POST['date-m'].$_POST['date-d'].$_POST['date-h'].'0000',
			'start' => $_POST['start'],
			'destination' => $_POST['destination'],
			'eta' => date('Y/m/d h:i:s', time()),
			'free_slots' => $_POST['free_slots'],
			'recurrence' => $_POST['recurrence'],
			'km' => 7,
			'ept' => 5,
			'added_date' => date('Y/m/d h:i:s', time()),
			'user_id' => $user_data['id']
		);
		//$this->load->view('dump', $new_trajet);
		$this->db->insert('Trajets', $new_trajet);
	}

	public function process() {
		$this->form_validation->set_rules('start', 'Départ', 'trim|required');
		$this->form_validation->set_rules('destination', 'Destination', 'trim|required');
		$this->form_validation->set_rules('start_precise', 'Départ', 'trim');
		$this->form_validation->set_rules('destination_precise', 'Départ', 'trim');
		$this->form_validation->set_rules('recurrence', 'Récurrence', 'trim');

		if ($this->form_validation->run() == false) {
			$this->load->view('templates/header');
			$this->load->view('trajet/create');
			$this->load->view('templates/footer');
		} else {
			if (strcmp($_POST['recurrence'], 'q')==0) {
				$this->process_trajet_quotidien();
			} else if (strcmp($_POST['recurrence'], 'h')==0) {
				$this->process_trajet_hebdo();
			} else if (strcmp($_POST['recurrence'], 'p')==0){ //trajet ponctuel
				$this->process_trajet_ponctuel();
			}
			redirect('dashboard/mes_trajets');
		}
	}

	public function verify_recurrence($str) {
		if (strcmp($str, 'p') == 0 || strcmp($str, 'm') == 0 || strcmp($str, 'h') == 0 || strcmp($str, 'q') == 0) {
			return true;
		} else {
			$this->form_validation->set_message('verify_recurrence', 'La case {field} n\'est pas valide');
			return false;
		}
	}

	public function trajet($trajet_id) {
		$data = $this->Trajet->getTrajetByID($trajet_id);
		$data['creator'] = $this->User->getUser($data['user_id']);
		$data['trajets_count'] = $this->User->getUserTrajetCount($data['user_id']);
		$data['logged_user'] = $this->User->getUser($this->session->userdata('logged_in')['id']);

		$this->load->view('templates/header');
		$this->load->view('trajet/trajet', $data);
		$this->load->view('templates/footer');
	}

	public function reserve($trajet_id, $user_id) {
		$data['trajet'] = $this->Trajet->getTrajetByID($trajet_id);
		$data['creator'] = $this->User->getUser($user_id);
		$data['customer'] = $this->User->getUser($this->session->userdata('logged_in')['id']);
		$data['dropdown_options'] = generate_dropdown_options($data['trajet']['free_slots']);
		$this->load->view('templates/header');
		$this->load->view('reservation/payment', $data);
		$this->load->view('templates/footer');
	}

	public function delete($trajet_id, $origin_url) {
		$result = $this->Trajet->deleteTrajet($trajet_id);
		$link = prep_url(base64_decode(urldecode($origin_url)));

		if ($result == false)
			$this->session->set_flashdata('delete_msg', '<div class="notification is-danger">Le trajet n\'a pas pu être supprimé. Veuillez réessayer ultérieurement.</div>');
		else
			$this->session->set_flashdata('delete_msg', '<div class="notification is-success">Le trajet a bien été supprimé.</div>');
		redirect($link);
	}

	public function edit($trajet_id, $origin_url) {

		$data['trajet'] = $this->Trajet->getTrajetByID($trajet_id);
		$data['origin'] = $link;
		$this->load->view('templates/header');
		$this->load->view('templates/footer');
		//redirect($link);
	}

	public function update($origin_url) {
		$uri = prep_url(base64_decode(urldecode($origin_url)));
		$trajet_id = $_POST['trajet_id'];
		$trajet['start'] = $_POST['start'];
		$trajet['destination'] = $_POST['destination'];
		$trajet['departure'] = $_POST['date-y'].$_POST['date-m'].$_POST['date-d'].$_POST['date-h'].'0000';
		$trajet['free_slots'] = $_POST['free_slots'];

		$this->Trajet->update($trajet_id, $trajet);
		$this->session->set_flashdata('edit_msg', '<div class="notification is-success">Le trajet a bien été modifié.</div>');
		redirect($uri);
	}

	public function edit_modal($trajet_id, $origin_url) {
		$data['trajet'] = $this->Trajet->getTrajetByID($trajet_id);
		$data['origin'] = $origin_url;
		$this->load->view('trajet/modal_edit', $data);
	}

}
