<div class="column is-7">
  <div class="notification is-info">
    <div class="columns">
      <div class="column is-10">
        <h2 class="title">Mes trajets</h2>
      </div>
      <div class="column is-2">
        <center>
          <a class="button is-white is-outlined" href="<?=base_url('trajets/create')?>">
            <span>Créer</span>
            <span class="icon is-small">
              <i class="fa fa-plus"></i>
            </span>
          </a>
        </center>
      </div>
    </div>
    <?=$this->session->flashdata('delete_msg');?>
    <?=$this->session->flashdata('edit_msg');?>
