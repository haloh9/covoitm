<section class="section">
  <div class="container">
    <div class="heading">
      <h1 class="title">Resultats</h1>
      <h2 class="subtitle">
      </h2>
    </div>
  </div>
</section>
<div class="container">
  <div class="columns">
    <div class="column is-1"></div>
    <div class="column auto">
      <div class="notification is-warning">
        Aucun résultat pour votre recherche !
        <br><br>
        Essayez d'élargir vos critères de recherche pour obtenir plus de résultats.
      </div>
      <br><br>
      <center><a class="button is-medium is-outlined is-primary" href="<?php echo base_url() ?>">Retourner à la recherche</a></center>
    </div>
    <div class="column is-1"></div>
  </div>
</div> 
