  <div class="column is-7">
    <div class="notification is-primary">
        <h1 class="title">
          Mon profil
        </h1>
        <div class="subtitle">
          Modification des informations personnelles
        </div>
        <?php echo form_open('dashboard/profil'); ?>
          <?php echo $this->session->flashdata('profil_msg'); ?>

          <div class="field is-horizontal">
            <div class="field-body">
              <div class="field is-grouped">
                <div class="control">
                  <div class="select is-medium">
                    <select name="gender">
                      <?php if ($user['gender'] == 0): ?>
                      <option selected="selected" value="0" >Homme</option>
                      <option value="1">Femme</option>
                      <?php elseif($user['gender'] == 1): ?>
                      <option value="0" >Homme</option>
                      <option selected="selected" value="1">Femme</option>
                      <?php endif; ?>
                    </select>
                  </div>
                </div>
              </div>
              <div class="field">
                <p class="control is-expanded has-icon has-icon-left">
                  <input class="input is-primary is-medium" type="text" name="last_name" value="<?php echo $user['last_name']?>" placeholder="Nom de famille">
                  <span class="icon is-medium">
                    <i class="fa fa-user"></i>
                  </span>
                </p>
                <p class="help is-danger"><?php echo form_error('last_name'); ?></p>
              </div>
              <div class="field">
                <p class="control is-expanded has-icon has-icon-left">
                  <input class="input is-primary is-medium" type="text" name="first_name" value="<?php echo $user['first_name']?>" placeholder="Prénom">
                  <span class="icon is-medium">
                    <i class="fa fa-user-o"></i>
                  </span>
                </p>
                <p class="help is-danger"><?php echo form_error('first_name'); ?></p>
              </div>

            </div>
          </div>

          <div class="field is-horizontal">
            <div class="field-body">
              <div class="field">
                <p class="control is-expanded has-icon has-icon-left">
                  <input class="input is-primary is-medium" type="email" name="email" value="<?php echo $user['email']?>" placeholder="Adresse e-mail" >
                  <span class="icon is-medium">
                    <i class="fa fa-envelope"></i>
                  </span>
                </p>
                <p class="help is-danger"><?php echo form_error('email'); ?></p>
                <p class="help">Si vous changez votre adresse mail, votre compte devient <strong>non verifié</strong>, un message contenant un lien de vérification vous sera alors envoyé à votre nouvelle adresse.</p>
              </div>
              <div class="field">
                <p class="control is-expanded has-icon has-icon-left">
                  <input class="input is-primary is-medium" type="tel" name="phone" value="<?php echo $user['phone']?>" placeholder="Numéro de téléphone">
                  <span class="icon is-medium">
                    <i class="fa fa-mobile"></i>
                  </span>
                </p>
                <p class="help is-danger"><?php echo form_error('phone'); ?></p>
              </div>
            </div>
          </div>

          <div class="field">
            <p class="control is-expanded has-icon has-icon-left">
              <input class="input is-primary is-medium" type="number" name="birth_year" value="<?php echo $user['birth_year']?>" style="width: 40%;">
              <span class="icon is-medium">
                <i class="fa fa-birthday-cake"></i>
              </span>
            </p>
            <p class="help is-danger"><?php echo form_error('birth_year'); ?></p>
          </div>

          <div class="field is-horizontal">
            <div class="field-body">
              <div class="field">
                <div class="control">
                  <textarea class="textarea" name="bio" placeholder="Dites nous un peu sur vous..."><?php echo $user['bio']?></textarea>
                </div>
              </div>
            </div>
          </div>

          <div class="field">
            <div class="subtitle">
              Type de compte
            </div>
            <p class="control">
              <span class="select is-medium">
                <select name="role">
                  <?php if ($user['role'] == 0): ?>
                  <option selected="selected" value="0">Conducteur</option>
                  <option value="1">Voyageur</option>
                  <?php elseif($user['role'] == 1): ?>
                  <option value="0">Conducteur</option>
                  <option selected="selected" value="1">Voyageur</option>
                  <?php endif; ?>
                </select>
              </span>
            </p>
          </div>

          <div class="field">
            <div class="control">
              <center><button type="submit" class="button is-success is-large">Mettre à jour mon compte</button></center>
            </div>
          </div>

        <?php echo form_close(); ?>
    </div>
  </div>
</div>
