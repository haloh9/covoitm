<?php if ($questions_trajet): ?>
  <?php foreach ($questions_trajet as $question): ?>
    <article class="media">
      <figure class="media-left">
        <p class="image is-64x64">
          <img src="<?php echo $question['avatar']?>">
        </p>
      </figure>
      <div class="media-content">
        <div class="content">
            <a href="<?php echo base_url('profil/user/'.$question['user_id'])?>"> <span style="color: #00d1b2;"><?php echo $question['poster'] ?></span></a>
            <br>
            <small><?php echo $question['qa_text'] ?></small>
        </div>
        <small>@</small> <small><?php echo $question['qa_timestamp'] ?></small>
      </div>
    </article>
  <?php endforeach; ?>
<?php else: ?>
  <div>Aucune question ajoutée</div>
<?php endif; ?>
