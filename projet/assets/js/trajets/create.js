//When the document DOM is ready
$(function() {
  current = 'p';
  dayNames = ["Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"];
  dayID = ["monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"];
  updateContent(current);
});

function changeTab(tab) {
  if (tab.localeCompare(current) == 0)
    return;

  if (tab.localeCompare('q') == 0) {
    $("#q").addClass("is-active");
    $("#h").removeClass("is-active");
    $("#m").removeClass("is-active");
    $("#p").removeClass("is-active");
    current = 'q';
  } else if (tab.localeCompare('h') == 0) {
    $("#q").removeClass("is-active");
    $("#h").addClass("is-active");
    $("#m").removeClass("is-active");
    $("#p").removeClass("is-active");
    current = 'h';
  } else if (tab.localeCompare('m') == 0) {
    $("#q").removeClass("is-active");
    $("#h").removeClass("is-active");
    $("#m").addClass("is-active");
    $("#p").removeClass("is-active");
    current = 'm';
  } else if (tab.localeCompare('p') == 0) {
    $("#q").removeClass("is-active");
    $("#h").removeClass("is-active");
    $("#m").removeClass("is-active");
    $("#p").addClass("is-active");
    current = 'p';
  } else
    console.error("Error on recurrence");

  updateContent(current);
}

function ySelector(id) {
  var currentTime = new Date();
  var ymdSelector = "<div class=\"field is-narrow\">";
  ymdSelector += "<p class=\"control\">";
  ymdSelector += "<span class=\"select\" id=\""+id+"\"><select name=\""+id+"\"><option value=\"-1\">Année</option>";
  for (var i = 0; i < 10; i++) {
    year = parseInt(currentTime.getFullYear())+i;
    ymdSelector += "<option value=\""+year+"\">"+year+"</option>";
  }
  ymdSelector += "</select></span></p></div>";
  return ymdSelector;
}

function mSelector(id) {
  var ymdSelector = "<div class=\"field is-narrow\">";
  ymdSelector += "<p class=\"control\">";
  ymdSelector += "<span class=\"select\" id=\""+id+"\"><select name=\""+id+"\"><option value=\"-1\">Mois</option>";
  for (var i = 1; i <= 12; i++) {
    if (i<10)
      val = "0"+i;
    else
      val = i;
    ymdSelector += "<option value=\""+val+"\">"+i+"</option>";
  }
  ymdSelector += "</select></span></p></div>";
  return ymdSelector;
}

function dSelector(id) {
  var ymdSelector = "<div class=\"field is-narrow\">";
  ymdSelector += "<p class=\"control\">";
  ymdSelector += "<span class=\"select\" id=\""+id+"\"><select name=\""+id+"\"><option value=\"-1\">Jour</option>";
  for (var i = 1; i <= 31; i++) {
    if (i<10)
      val = "0"+i;
    else
      val = i;
    ymdSelector += "<option value=\""+val+"\">"+i+"</option>";
  }
  ymdSelector += "</select></span></p></div>";
  return ymdSelector;
}

function ymdSelector(dayID, monthID, yearID) {
  var ymdSelector = dSelector(dayID);
  ymdSelector += mSelector(monthID);
  ymdSelector += ySelector(yearID);

  return ymdSelector;
}

function hSelector(id) {
  var selector = "<div class=\"field is-narrow\">";
  selector += "<p class=\"control\">"
  selector += "<span class=\"select\" id=\""+id+"\"><select name=\""+id+"\"><option value=\"-1\">Heure</option>";
  for (var i = 0; i < 24; i++) {
    if (i<10) {
      hour = "0"+i+":00";
      val = "0"+i;
    } else {
      hour = i+":00";
      val = ""+i;
    }
    selector += "<option value=\""+val+"\">"+hour+"</option>";
  }
  selector += "</select></span></p></div>";
  return selector;
}

function weekdaySelector() {
  var selector = "";
  for (var i = 0; i < dayNames.length; i++) {
    selector += "<div class=\"field is-horizontal\"><div class=\"field-label\" style=\"\">";
    selector += "<label class=\"label\" style=\"width: 90px;\">"+dayNames[i]+"</label></div>";
    selector += "<div class=\"field-body\"><div class=\"field is-narrow\"><div class=\"control\">"
    selector += "<input is-small type=\"checkbox\" name=\""+dayID[i]+"\" value=\"checked\" style=\"vertical-align: bottom;\">";
    selector += "</div></div>";
    selector += hSelector("date-h-"+dayID[i]);
    selector += "</div></div>";
  }
  return selector;
}

function updateContent(mode) {
  if (arguments.length ==2 ) {
    node = "#"+arguments[1];
  } else {
    node = "#tab-content";
  }
  switch (mode) {
    case 'p':
      var tabContent = "<br><div class=\"field is-horizontal\"><div class=\"field-label is-normal\">";
      tabContent += "<label class=\"label\">Date</label>";
      tabContent += "</div><div class=\"field-body\">";
      tabContent += ymdSelector("date-d", "date-m", "date-y");
      tabContent += "</div></div>";

      tabContent += "<div class=\"field is-horizontal\"><div class=\"field-label is-normal\">";
      tabContent += "<label class=\"label\">À</label>";
      tabContent += "</div><div class=\"field-body\">";
      tabContent += hSelector("date-h");
      tabContent += "</div></div>";

      $(node).html(tabContent);
      $("#recurrence").val("p");
      break;
    case 'q':
      var tabContent = "<br><div class=\"field is-horizontal\"><div class=\"field-label is-normal\">";
      tabContent += "<label class=\"label\">Premier jour</label>";
      tabContent += "</div><div class=\"field-body\">";
      tabContent += ymdSelector("date-d-start", "date-m-start", "date-y-start");
      tabContent += "</div></div>";

      tabContent += "<div class=\"field is-horizontal\"><div class=\"field-label is-normal\">";
      tabContent += "<label class=\"label\">Dernier jour</label>";
      tabContent += "</div><div class=\"field-body\">";
      tabContent += ymdSelector("date-d-end", "date-m-end", "date-y-end");
      tabContent += "</div></div>";

      tabContent += "<div class=\"field is-horizontal\"><div class=\"field-label is-normal\">";
      tabContent += "<label class=\"label\">À</label>";
      tabContent += "</div><div class=\"field-body\">";
      tabContent += hSelector("date-h");
      tabContent += "</div></div>";

      $(node).html(tabContent);
      $("#recurrence").val("q");
      break;
    case 'h':
      var tabContent = "<br><div class=\"columns\">";
      tabContent += "<div class=\"column is-half\">"
      tabContent += weekdaySelector();
      tabContent += "</div><div class=\"column is-half\">";
      /* Vertical
      tabContent += "<div class=\"field is-horizontal\"><div class=\"field-label is-normal\">";
      tabContent += "<label class=\"label\">Premier jour</label>";
      tabContent += "</div><div class=\"field-body\">";
      tabContent += ymdSelector("date-d-start", "date-m-start", "date-y-start");
      tabContent += "</div></div>";

      tabContent += "<div class=\"field is-horizontal\"><div class=\"field-label is-normal\">";
      tabContent += "<label class=\"label\">Premier jour</label>";
      tabContent += "</div><div class=\"field-body\">";
      tabContent += ymdSelector("date-d-end", "date-m-end", "date-y-end");
      tabContent += "</div></div>";
      tabContent += "";*/

      tabContent += "<div class=\"field\">";
      tabContent += "<label class=\"label\">Premier jour</label>";
      tabContent += "<div class=\"field is-horizontal\">";
      tabContent += ymdSelector("date-d-start", "date-m-start", "date-y-start");
      tabContent += "</div></div>";

      tabContent += "<div class=\"field\">";
      tabContent += "<label class=\"label\">Dernier</label>";
      tabContent += "<div class=\"field is-horizontal\">";
      tabContent += ymdSelector("date-d-end", "date-m-end", "date-y-end");
      tabContent += "</div></div>";
      tabContent += "</div></div>";

      $(node).html(tabContent);
      $("#recurrence").val("h");
      break;

    case 'm': //pas utile
      var tabContent = "<p class=\"control\"><center>";
      tabContent += dSelector("date-d");
      tabContent += "</center></p>";

      $(node).html(tabContent);
      $("#recurrence").val("m");
      break;
    default:

  }
}
