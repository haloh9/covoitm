<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller{

	function __construct() {
    parent::__construct();
		// On charge les models / helpers / librarys
		$this->load->model('User');
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->library('email');

		// On ne peut y acceder que si on est connecter, sinon on redirige vers la page de connexion
		if(!is_logged_in())
			redirect('auth/login');
  }

	public function index() {
		redirect('dashboard/profil');
	}

	public function profil(){
		$logged_user = $this->session->userdata('logged_in');
		$user_data = $this->User->getUser($logged_user['id']); // Récupere toutes les informations d'un utilisateur de la base de donnée
		$data['user'] = array(
	    'email' => $user_data['user_email'],
	    'first_name' => $user_data['user_first_name'],
	    'last_name' => $user_data['user_last_name'],
	    'gender' => $user_data['user_gender'],
	    'birth_year' => $user_data['user_birth_year'],
	    'phone' => $user_data['user_phone'],
	    'role' => $user_data['user_role'],
	    'avatar' => $user_data['user_avatar'],
	    'bio' => $user_data['user_bio'],
	    'balance' => $user_data['user_balance'],
	    'loyaltypoints' => $user_data['user_loyaltypoints'],
	    'created_at' => $user_data['user_created_at'],
			'updated_at' => $user_data['user_updated_at'],
	    'status' => $user_data['user_status']
  	);
		$status_compte = $user_data['user_status']; // 0 => non vérifié / 1 => vérifié
		if($_POST == null){
			$this->load->view('templates/header');
			$this->load->view('dashboard/menu');
			$this->load->view('dashboard/profil', $data);
			$this->load->view('templates/footer');
			return;
		}
		$this->form_validation->set_rules('first_name', 'prénom', 'trim|required|alpha_numeric_spaces|min_length[3]|max_length[25]|xss_clean');
		$this->form_validation->set_rules('last_name', 'nom', 'trim|required|alpha_numeric_spaces|min_length[3]|max_length[25]|xss_clean');
		$this->form_validation->set_rules('birth_year', 'année de naissance', 'required|greater_than[1900]|less_than[2018]');
		$this->form_validation->set_rules('phone', 'numéro de téléphone', 'numeric', array('numeric' => 'Erreur : "Numéro de telephone" est vide'));
		$this->form_validation->set_rules('email', 'adresse mail', 'trim|required|valid_email');
		$this->form_validation->set_rules('bio', 'ma biographie', 'trim');

		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('profil_msg', '<div class="notification is-danger">Aucun changement n\'a été effectué. Veuillez corriger les erreurs du formulaire.</div>');
			$this->load->view('templates/header');
			$this->load->view('dashboard/menu');
			$this->load->view('dashboard/profil', $data);
			$this->load->view('templates/footer');
		} else {
			// On verifie si l'utilisateur à modifier la champ 'mail'
			if($this->input->post('email') !=  $user_data['user_email']){
				//si c'est le cas on verifie que la nouvelle adresse mail n'existe pas dans la base de donnée
				if($this->User->isDuplicate_email($this->input->post('email'))){
					$this->session->set_flashdata('profil_msg', '<div class="notification is-danger">Cette adresse e-mail existe déjà dans nos registres. <br> Aucun changement n\'a été effectué.</div>');
					redirect('dashboard/profil');
				}
				//Envoyer le mail de vérification de la nouvelle adresse
				$status_compte = 0;
				$this->email->from('covoitmpia2017@gmail.com', 'CovoitM');
				$this->email->to($this->input->post('email'));
				$this->email->subject('[CovoitM]: verification de la nouvelle adresse email');
				$this->email->message('Vous avez recemment modifier votre adresse email sur le site CovoitM<br/>veuillez la verifier en cliquant sur le lien :'. 		base_url("verify/process/".$logged_user['id']."/".md5($this->input->post('email'))). '<br>A bientot sur CovoitM');
				$this->email->send();
			}
			$user_data = array(
				'email' => $this->input->post('email'),
				'first_name' => $this->input->post('first_name'),
				'last_name' => $this->input->post('last_name'),
				'gender' => $this->input->post('gender'),
				'birth_year' => $this->input->post('birth_year'),
				'phone' => $this->input->post('phone'),
				'role' => $this->input->post('role'),
				'bio' => $this->input->post('bio'),
				'status' => $status_compte
			);
			$this->User->update_profil_user($user_data, $logged_user['id']);
			// Envoie de message de confirmation
			$this->session->set_flashdata('profil_msg', '<div class="notification is-success">Modifications éffectuées avec succès</div>');
			redirect('dashboard/profil');
		}
	}

	public function update_password() {
		$logged_user = $this->session->userdata('logged_in');
		// Récupere toutes les informations d'un utilisateur de la base de donnée
		$user_data = $this->User->getUser($logged_user['id']);
		if($_POST == null){
			$this->load->view('templates/header');
			$this->load->view('dashboard/menu');
			$this->load->view('dashboard/update_password');
			$this->load->view('templates/footer');
			return;
		}
		$this->form_validation->set_rules('new_password', 'nouveau mot de passe', 'trim|required');
		$this->form_validation->set_rules('confirm_new_password', 'confirmation du nouveau mot de passe', 'trim|required|matches[new_password]');

		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('update_pass_err_msg', '<div class="notification is-danger">Veuillez corriger les erreurs du formulaire</div>');
			$this->load->view('templates/header');
			$this->load->view('dashboard/menu');
			$this->load->view('dashboard/update_password');
			$this->load->view('templates/footer');
		} else {
			$this->User->update_password($logged_user['id'], $this->input->post('new_password'));
			$this->session->set_flashdata('update_pass_succ_msg', '<div class="notification is-success">Modifications effectuées avec succès</div>');
			$this->email->from('covoitmpia2017@gmail.com', 'CovoitM');
			$this->email->to($user_data['user_email']);
			$this->email->subject('[CovoitM]: changement de mot de passe');
			$this->email->message('Vous avez recement changer votre mot de passe depuis votre espace client CovoitM.<br>Votre nouveau mot de passe :'. $this->input->post('new_password') . '<br><br>A bientot sur CovoitM.');
			$this->email->send();
			$this->load->view('templates/header');
			$this->load->view('dashboard/menu');
			$this->load->view('dashboard/update_password');
			$this->load->view('templates/footer');
				}
	}

	public function mes_trajets() {
		$this->load->model('Trajet');
		$user_session = $this->session->userdata('logged_in');
		$mes_trajets = $this->Trajet->getTrajetsCreatedBy($user_session['id']);

		$this->load->view('templates/header');
		$this->load->view('dashboard/menu');
		$this->load->view('dashboard/mes_trajets_top');
		if (count($mes_trajets) < 1)
			$this->load->view('trajet/no_trajets');
		else {
			foreach ($mes_trajets as $trajet) {
				$data['trajet'] = $trajet;
				$data['creator'] =  $this->User->getUser($trajet['user_id']);
				$data['logged_user'] = $this->User->getUser($user_session['id']);
				$this->load->view('trajet/trajet_card', $data);
			}
		}
		$this->load->view('dashboard/mes_trajets_bottom');
		$this->load->view('templates/footer');
	}

	public function mes_reservations() {
		$this->load->model('Trajet');
		$user_session = $this->session->userdata('logged_in');
		$mes_reservations = $this->Trajet->getTrajetsReservedBy($this->session->userdata('logged_in')['id']);

		$this->load->view('templates/header');
		$this->load->view('dashboard/menu');
		$this->load->view('dashboard/mes_reservations_top');
		if (count($mes_reservations) < 1)
			$this->load->view('trajet/no_trajets');
		else {
			foreach ($mes_reservations as $trajet) {
				$data['trajet'] = $trajet;
				$data['creator'] =  $this->User->getUser($trajet['user_id']);
				$data['logged_user'] = $this->User->getUser($user_session['id']);
				$this->load->view('trajet/trajet_card', $data);
			}
		}
		$this->load->view('dashboard/mes_reservations_bottom');
		$this->load->view('templates/footer');
	}

	public function mes_avis() {
		$this->load->model('Review');
		$logged_user = $this->session->userdata('logged_in');
		// Récupere toutes les informations à propos des avis données et recu de l'utilisateur connecté
		$data['reviews_received'] = $this->Review->getReviewsReceived($logged_user['id']);
		$data['reviews_posted'] = $this->Review->getReviewsPosted($logged_user['id']);
		$this->load->view('templates/header');
		$this->load->view('dashboard/menu');
		$this->load->view('dashboard/mes_avis', $data);
		$this->load->view('templates/footer');
	}

	public function mes_preferences(){
		$this->load->model('Preference');
		$logged_user = $this->session->userdata('logged_in');
		if($_POST == null){
			$preferences_data = $this->Preference->getPreference($logged_user['id']);
			$data['preferences'] = array(
				'cigarette' => $preferences_data['cigarette'],
				'pets' => $preferences_data['pets'],
				'music' => $preferences_data['music']
			);
			$this->load->view('templates/header');
			$this->load->view('dashboard/menu');
			$this->load->view('dashboard/mes_preferences', $data);
			$this->load->view('templates/footer');
			return;
		}
		$data['preferences'] = array(
			'cigarette' => 	$this->input->post('cigarette'),
			'pets' => $this->input->post('pets'),
			'music' => 	$this->input->post('music')
		);
		$this->Preference->setPreference($data['preferences'], $logged_user['id']);
		$this->session->set_flashdata('mes_preferences_msg', '<div class="notification is-success">Modifications éffectuées avec succès</div>');
		redirect('dashboard/mes_preferences');
	}

	public function mes_notifications() {
		$this->load->model('Notification');
		$logged_user = $this->session->userdata('logged_in');
		$data['new_notif'] = $this->Notification->getNotifs($logged_user['id'], false);
		$data['viewed_notif'] = $this->Notification->getNotifs($logged_user['id'], true);
		$this->load->view('templates/header');
		$this->load->view('dashboard/menu');
		$this->load->view('dashboard/mes_notifications', $data);
		$this->load->view('templates/footer');
		$this->Notification->makeNotifsViewed($logged_user['id']);
	}

	public function crediter() {
		$this->load->view('templates/header');
		$this->load->view('dashboard/menu');
		$this->load->view('dashboard/crediter');
		$this->load->view('templates/footer');
	}

	public function logout(){
		$logged_user = $this->session->userdata('logged_in');
		$this->User->update_last_seen($logged_user['id']);
		$this->session->unset_userdata('logged_in');
		session_destroy();
		redirect('');
	}
}
