	<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {

	function __construct(){
    parent::__construct();
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->helper('string');
		$this->load->library('form_validation');
		$this->load->library('email');
		/**
		** On ne peut y acceder que si on est pas connecter, sinon on redirige vers la page principal
		**/
		if(is_logged_in())
			redirect('', 'refresh');
  }

	public function index()
	{
			$this->load->view('templates/header');
			$this->load->view('register/index');
			$this->load->view('templates/footer');
	}

	function verify_recaptcha($recaptcha){
    $response = $this->recaptcha->verifyResponse($recaptcha);
    if (isset($response['success']) and $response['success'] === true)
        return true;
    else {
      $this->form_validation->set_message('verify_recaptcha', 'reCAPTCHA invalide, essayer encore une fois');
      return false;
    }
  }

	public function process() {
		$this->load->model('user');
		//set validation rules
		$this->form_validation->set_rules('gender', 'genre', 'required|is_natural', array('is_natural' => 'Erreur : "Civilité" est vide'));
		$this->form_validation->set_rules('role', 'role', 'required|in_list[0,1]', array('in_list' => 'Erreur : "Role" est virde'));
		$this->form_validation->set_rules('first_name', 'prénom', 'trim|required|alpha_numeric_spaces|min_length[3]|max_length[25]|xss_clean');
		$this->form_validation->set_rules('last_name', 'nom', 'trim|required|alpha_numeric_spaces|min_length[3]|max_length[25]|xss_clean');
		$this->form_validation->set_rules('email', 'adresse mail', 'trim|required|valid_email');
		$this->form_validation->set_rules('password', 'mot de passe', 'trim|required');
		$this->form_validation->set_rules('confirm_password', 'confirmation du mot de passe', 'trim|required|matches[password]');
		$this->form_validation->set_rules('birth_year', 'année de naissance', 'required|greater_than[1900]|less_than[2018]');
		$this->form_validation->set_rules('phone', 'numéro de téléphone', 'required|regex_match[/^[0-9]{10}$/]');
		$this->form_validation->set_rules('g-recaptcha-response','Captcha','callback_verify_recaptcha');

		//validate form input
    if ($this->form_validation->run() == FALSE){
				$this->load->view('templates/header');
				$this->load->view('register/index');
				$this->load->view('templates/footer');
		} else{
				if($this->User->isDuplicate_email($this->input->post('email'))){
					$this->session->set_flashdata('error_email_message', 'Adresse e-mail déjà existante');
					$this->load->view('templates/header');
					$this->load->view('register/index');
					$this->load->view('templates/footer');
					return;
				}
				if($this->input->post('term_and_conditions') !== 'checked'){
					$this->session->set_flashdata('error_tec_message', 'Veuillez accepter les conditions');
					$this->load->view('templates/header');
					$this->load->view('register/index');
					$this->load->view('templates/footer');
					return;
				}
				$data = array(
					'password' => $this->input->post('password'),
					'email' => $this->input->post('email'),
					'first_name' => $this->input->post('first_name'),
					'last_name' => $this->input->post('last_name'),
					'gender' => $this->input->post('gender'),
					'birth_year' => $this->input->post('birth_year'),
					'phone' => $this->input->post('phone'),
					'role' => $this->input->post('role')
				);
				// insert form data form into database
				$this->User->setUser($data);

				// Initialisation des préférences de l'utilisateur
				$this->load->model('Preference');
				$new_user_id = $this->User->getIdByMail($data['email']);
				$this->Preference->initPreference($new_user_id);
				// Envoie de mail de confirmation
				$this->email->from('covoitmpia2017@gmail.com', 'CovoitM');
				$this->email->to($this->input->post('email'));
				$this->email->subject('[CovoitM]: verification de l\'adresse email');
				$this->email->message('Cliquez sur ce lien a fin de verifier votre adresse email '. base_url("verify/process/".$new_user_id."/".md5($this->input->post('email'))). '<br>A bientot sur CovoitM');
				$this->email->send();
				$this->load->view('templates/header');
				$this->load->view('register/success', array('email' => $this->input->post('email')));
				$this->load->view('templates/footer');
		}
	}


}
