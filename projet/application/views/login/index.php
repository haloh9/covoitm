<section class="section">
  <div class="container">
    <div class="heading">
      <h1 class="title">Connexion</h1>
    </div>
  </div>
</section>
<div class="columns">
  <div class="column is-one-third"></div>
  <div class="column auto">
    <div class="content">
      <div class="box">
           <?php echo form_open('auth/login'); ?>
             <h2 class="subtitle">Veuillez saisir vos identifiants</h2>
             <?php echo $this->session->flashdata('error_login_message');?>

             <div class="field">
               <div class="control is-expanded has-icon has-icon-left">
                 <input class="input is-primary is-medium" type="email" size="20" value="<?php echo set_value('email') ?>" name="email" placeholder="Adresse e-mail"/>
                 <span class="icon is-medium">
                   <i class="fa fa-envelope"></i>
                 </span>
               </div>
               <?php echo form_error('email', '<p class="help is-danger">', '</p>'); ?>
             </div>

             <div class="field">
               <div class="control is-expanded has-icon has-icon-left">
                 <input class="input is-primary is-medium" type="password" name="password" placeholder="Mot de passe">
                 <span class="icon is-medium">
                   <i class="fa fa-key"></i>
                 </span>
               </div>
             </div>

             <div class="field">
               <p class="control">
                 <label class="checkbox"><?php echo form_checkbox(array("id"=>"remember_me", 'name' => "remember_me", 'value' => "checked")); ?>Se souvenir de moi</label>
               </p>
            </div>

            <div class="field is-grouped">
              <p class="control">
                <a class="button is-large" href="<?php echo base_url('auth/reset_password')?>">
                  Mot de passe oublié
                </a>
              </p>
              <p class="control">
                <button type="submit" class="button is-success is-large">
                  Se connecter
                </button>
              </p>
            </div>
           <?php echo form_close(); ?>
      </div>
    </div>
  </div>
  <div class="column is-one-third"></div>
</div>
