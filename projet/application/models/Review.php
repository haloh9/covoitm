<?php
  class Review extends CI_Model {
    public function __construct() {
      $this->load->database();
    }
    
    public function setReview($review_data){
        $resultat= $this->db->insert("Reviews" , $review_data);
    }

    public function setStars($review_stars){
    	 $resultat= $this->db->insert("Reviews" , $review_stars);
    }

    public function getReviewsPosted($user_id){
      $this->db->from('Reviews');
      $this->db->where(array('user_id' => $user_id),0,20);
      $this->db->order_by("review_created_at", "DESC");
      $query = $this->db->get();
      $rows = $query->result_array();

      if($rows){
        foreach ($rows as $row) {
          $row['poster'] = $this->User->getUser($row['user_id'])['user_first_name'].' '.$this->User->getUser($row['user_id'])['user_last_name'];
          $row['avatar'] = $this->User->getUser($row['user_id'])['user_avatar'];
          $results[] = $row;
        }
        return $results;
      } else
        return false;
    }

    public function getReviewsReceived($user_id){
      $this->db->from('Reviews');
      $this->db->where(array('user_reviewed' => $user_id),0,20);
      $this->db->order_by("review_created_at", "DESC");
      $query = $this->db->get();
      $rows = $query->result_array();
      if($rows){
        foreach ($rows as $row) {
          $row['poster'] = $this->User->getUser($row['user_id'])['user_first_name'].' '.$this->User->getUser($row['user_id'])['user_last_name'];
          $row['avatar'] = $this->User->getUser($row['user_id'])['user_avatar'];
          $results[] = $row;
        }
        return $results;
      } else
        return false;
    }

  }
