<!-- Our JS -->
<script src="<?php echo base_url('assets/js/trajets/create.js'); ?>" type="text/javascript"></script>
<script type="text/javascript">
  function updatePrices() {
      count = parseInt($("#slots option:selected").val());
      price = parseInt($("#priceperslot").val());
      pretotal = parseFloat(count*price).toFixed(2);
      $("#pricexslots").text(pretotal+' €');
      total = parseFloat(pretotal)+0.5;
      total = parseFloat(total).toFixed(2);
      $("#total").text(total+' €');

      console.log(parseFloat($("#balance").text())+' '+total);
      if (parseFloat($("#balance").text()) < total) {
        $("#pay").attr("disabled", "disabled");
        $("#insufficient").html("Votre solde est insuffisant pour réserver ce trajet.<br><a href=\"<?=base_url('dashboard/crediter');?>\">Changer cela.</a>");
      } else {
        if ($("#pay").attr("disabled")) {
          $("#pay").removeAttr("disabled");
        }
        $("#insufficient").html("");
      }
  }
  $(function() {
    updatePrices();
    $("#slots").on('change', function() {
      updatePrices();
    });
  });
</script>

<section class="section">
  <div class="container">
    <div class="heading">
      <h1 class="title">Réserver un trajet</h1>
      <h2 class="subtitle">Veuillez confirmer les informations relatives à votre reservation</h2>
    </div>
  </div>
</section>
<?php echo form_open('trajets/process_reservation'); ?>
<div class="columns">
  <div class="column is-2"></div>
  <div class="column is-4">
    <div class="content">
      <div class="box">
        <label class="label">Résumé du trajet</label>
        <input type="text" name="trajet_id" value="<?=$trajet['trajet_id'];?>" hidden>
        <input type="text" name="creator_id" value="<?=$creator['user_id'];?>" hidden>
        <table class="table is-stripped is-bordered">
          <tbody>
            <tr>
              <th><abbr title="Endroit de départ">Départ</abbr></th>
              <td><?=$trajet['start'];?></td>
            </tr>
            <tr>
              <th><abbr title="Endroit d'arrivée">Arrivée</abbr></th>
              <td><?=$trajet['destination'];?></td>
            </tr>
            <tr>
              <th><abbr title="Date de début du voyage">Date</abbr></th>
              <td><?=date_to_str($trajet['departure']);?></td>
            </tr>
            <tr>
              <th><abbr title="Places disponibles à la réservation">Places</abbr></th>
              <td><?=$trajet['free_slots'];?></td>
            </tr>
            <tr>
              <th>Distance</th>
              <td>7 km</td>
            </tr>
            <tr>
              <th>Conducteur</th>
              <td><a href="<?=base_url('profil/user/'.$creator['user_id']);?>"><?=$creator['user_first_name'].' '.$creator['user_last_name'];?></a></td>
            </tr>
          </tbody>
        </table>
        <center>
          <a href="<?php echo base_url('trajets/trajet/'.$trajet['trajet_id']); ?>" class="button is-dark is-large">
            <span class="icon">
              <i class="fa fa-caret-left"></i>
            </span>
            <span>Retourner au détail du trajet</span>
          </a>
        </center>
      </div>
    </div>
  </div>
  <div class="column is-4">
    <div class="box">
      <div class="field">
        <label class="label">Solde actuel</label>
        <center><h1 id="balance" class="subtitle is-5 is-success"><?=$customer['user_balance']?> €</h1></center>
      </div>
      <div class="field">
        <label class="label">Nombre de places souhaitées</label>
        <p class="control">
          <center>
            <span class="select">
              <?php echo form_dropdown('reserve_slots', $dropdown_options, '1', array('id' => 'slots')); ?>
            </span>
          </center>
        </p>
      </div>
      <div class="field">
        <input type="text" id="priceperslot" value="<?=$trajet['ept']?>" hidden>
        <table class="table">
          <tbody>
            <tr>
              <th><abbr title="Cotisation totale des places"><span class="icon">
                <i class="fa fa-user-plus"></i>
              </span></abbr></th>
              <td id="pricexslots"></td>
            </tr>
            <tr>
              <th><abbr title="Frais de gestion"><span class="icon">
                <i class="fa fa-cogs"></i>
              </span></abbr></th>
              <td>0.50 €</td>
            </tr>
            <tr>
              <th><abbr title="Total"><span class="icon">
                <i class="fa fa-eur"></i>
              </span></abbr></th>
              <td><strong  id="total">5.50 €</strong>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      <p class="control">
        <center>
          <button id="pay" type="submit" class="button is-primary is-large">
            <span>Payer et réserver</span>
            <span class="icon">
              <i class="fa fa-caret-right"></i>
            </span>
          </button>
          <div id="insufficient" class="help"></div>
        </center>
      </p>
    </form>
  </div>
</div>
<div class="column is-2"></div>
</div>
