/** Declaration ipnut hidden **/
var cigarette_input =  document.getElementById("cigarette");
var pets_input =  document.getElementById('pets');
var music_input =  document.getElementById("music");

/** Declaration img element **/
var cig_neute = document.getElementById('cig_neute');
var cig_true = document.getElementById('cig_true');
var cig_false = document.getElementById('cig_false');
var pets_neute = document.getElementById('pets_neute');
var pets_true = document.getElementById('pets_true');
var pets_false = document.getElementById('pets_false');
var msc_neute = document.getElementById('msc_neute');
var msc_true = document.getElementById('msc_true');
var msc_false = document.getElementById('msc_false');

// Initialisation préferences user
changeCigPreference(cigarette_input.value);
changePetsPreference(pets_input.value);
changeMscPreference(music_input.value);

function changeCigPreference(pref) {
  cig_true.style.color = "#A4A4A4";
  cig_false.style.color = "#A4A4A4";
  cig_neute.style.color = "#A4A4A4";
  if (pref == 'neute') {
    cig_neute.style.color = "#00d1b2";
    cigarette_input.value = 'neute';
  } else if (pref == 'true') {
    cig_true.style.color = "#00d1b2";
    cigarette_input.value = 'true';
  } else if (pref == 'false') {
    cig_false.style.color = "#00d1b2";
    cigarette_input.value = 'false';
  }
}

function changePetsPreference(pref) {
  pets_true.style.color = "#A4A4A4";
  pets_false.style.color = "#A4A4A4";
  pets_neute.style.color = "#A4A4A4";
  if (pref == 'neute') {
    pets_neute.style.color = "#00d1b2";
    pets_input.value = 'neute';
  } else if (pref == 'true') {
    pets_true.style.color = "#00d1b2";
    pets_input.value = 'true';
  } else if (pref == 'false') {
    pets_false.style.color = "#00d1b2";
    pets_input.value = 'false';
  }
}

function changeMscPreference(pref) {
  msc_true.style.color = "#A4A4A4";
  msc_false.style.color = "#A4A4A4";
  msc_neute.style.color = "#A4A4A4";
  if (pref == 'neute') {
    msc_neute.style.color = "#00d1b2";
    music_input.value = 'neute';
  } else if (pref == 'true') {
    msc_true.style.color = "#00d1b2";
    music_input.value = 'true';
  } else if (pref == 'false') {
    msc_false.style.color = "#00d1b2";
    music_input.value = 'false';
  }
}
