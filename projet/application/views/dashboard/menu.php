<section class="section">
  <div class="container">
    <div class="heading">
      <h1 class="title">Espace membre</h1>
      <h2 class="subtitle">
      </h2>
    </div>
  </div>
</section>
<div class="columns">
  <div class="column is-1"></div>
  <div class="column is-3">
    <div class="box is-fix">
        <aside class="menu">
          <p class="menu-label">
            General
          </p>
          <ul class="menu-list">
            <li><a href="<?php echo base_url('dashboard/profil')?>">Mon profil</a></li>
            <li><a href="<?php echo base_url('dashboard/update_password')?>">Modifier mon mot de passe</a></li>
            <li><a href="<?php echo base_url('dashboard/mes_trajets')?>" >Mes trajets</a></li>
            <li><a href="<?php echo base_url('dashboard/mes_reservations')?>">Mes réservations</a></li>
            <li><a href="<?php echo base_url('dashboard/mes_preferences')?>">Mes préferences</a></li>
            <li><a href="<?php echo base_url('dashboard/mes_notifications')?>">Mes notifications</a></li>
            <li><a href="<?php echo base_url('dashboard/mes_avis')?>">Mes évaluations</a></li>
          </ul>
          <p class="menu-label">
            Portfeuille (Solde : <strong><?php echo get_balance()?>€</strong>)
          </p>
          <ul class="menu-list">
            <li><a href="<?php echo base_url('dashboard/crediter')?>">Crediter</a></li>
            <li><a href="<?php echo base_url('dashboard/profil')?>">Historique</a></li>
          </ul>
          </aside>
        </div>
  </div>
