<?php
  class Question extends CI_Model {
    public function __construct() {
      $this->load->database();
    }

    public function getTrajetQuestions($trajet_id){
      $this->db->from('Q_A');
      $this->db->where(array('trajet_id' => $trajet_id));
      $query = $this->db->get();
      $rows = $query->result_array();

      if($rows){
        foreach ($rows as $row) {
          $row['poster'] = $this->User->getUser($row['user_id'])['user_first_name'].' '.$this->User->getUser($row['user_id'])['user_last_name'];
          $row['avatar'] = $this->User->getUser($row['user_id'])['user_avatar'];
          $results[] = $row;
        }
        return $results;
      } else
        return false;

    }

    public function setQuestion($question_data){
      return $this->db->insert("Q_A" , $question_data);
    }



  }
