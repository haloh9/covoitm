<script type="text/javascript" src="<?php echo base_url() ?>assets/js/profil/user.js"></script>
<script src="<?php echo base_url('assets/js/trajets/trajet_card.js') ?>" type="text/javascript"></script>

<script>
function profil_review_update(){
  $.ajax({
    type: 'GET',
    data: $data,
    url: '<?php echo base_url("/reviews/get_profil_view/".$user['id']);?>',
    success: function($data){
      $('#avis_ajax').html($data);
    }
  });
}

$('document').ready(function(){
  setInterval('profil_review_update();',500);
});

function addReview(){
  document.getElementById('add_review').className = "modal is-active";
  $.ajax({
    type: 'GET',
    data: $data,
    url: '<?php echo base_url("/reviews/get_add_form_view");?>',
    success: function($data){
      $('#content_form_review').html($data);
    }
  });
}
function hide_modal_review(){
  document.getElementById('add_review').className = "modal";
}
</script>

<section class="section">
  <div class="container">
    <div class="heading">
      <h1 class="title">Profil</h1>
      <h2 class="subtitle">
      </h2>
    </div>
  </div>
</section>
<div class="container box">

  <div class="profile-heading">
    <div class="columns">
      <div class="column is-2">
        <div class="image is-128x128 avatar">
          <img class="profil_img" src="<?php echo $user['avatar'] ?>">
        </div>
      </div>
      <div class="column is-4 name">
        <p>
          <span class="title is-bold"><?php echo $user['first_name'].' '.$user['last_name'] ?></span>
        </p>
        <p class="tagline">
          <?php echo "Age : ".$user['age'] ?><br>
          <?php echo "Membre depuis : ".$user['created_at'] ?>
        </p>
      </div>
      <div class="column is-3 followers has-text-centered">
        <p class="stat-key">Etat du compte</p>
        <?php if ($user['status']==0): ?>
        <span class="tag is-large is-danger">Non vérifié</span>
        <?php else: ?>
        <span class="tag is-large is-success">Vérifié</span>
        <?php endif; ?>
      </div>
      <div class="column is-3 likes has-text-centered">
        <p class="stat-key">Avis moyen (<small><?php echo $average_stars?>/5</small>)</p>
        <?php if ($average_stars>1 && $average_stars<4): ?>
          <span class="icon is-large">
            <i class="fa fa-star-half-o fa-5" style="color:#00d1b2"></i>
          </span>
        <?php elseif ($average_stars>= 4): ?>
          <span class="icon is-large">
            <i class="fa fa-star fa-5" style="color:#00d1b2"></i>
          </span>
        <?php else: ?>
          <span class="icon is-large">
            <i class="fa fa-star-o" style="color:#00d1b2"></i>
          </span>
        <?php endif; ?>
      </div>
    </div>
  </div>

  <div class="profile-options">
    <div class="tabs is-fullwidth">
      <ul>
        <li id="tab_profil_info" class="link is-active" onclick="changeTab('i')"><a><span class="icon"><i class="fa fa-info-circle"></i></span> <span>Informations</span></a></li>
        <li id="tab_profil_avis" class="link" onclick="changeTab('a')"><a><span class="icon"><i class="fa fa-address-card-o"></i></span> <span>Avis reçus</span></a></li>
        <li id="tab_profil_trajets" class="link" onclick="changeTab('t')"><a><span class="icon"><i class="fa fa-road"></i></span> <span>Trajets postés</span></a></li>
        <li id="tab_profil_contact" class="link" onclick="changeTab('c')"><a><span class="icon"><i class="fa fa-telegram"></i></span><span>Contact</span></a></li>
      </ul>
    </div>
  </div>

  <div id="contenu_profil_info" style="display: block;">
    <div  class="tile is-ancestor">
      <div class="tile is-4 is-vertical is-parent">
        <div class="tile is-child notification">
          <p class="title is-4"> <span class="tag is-medium is-dark"><?php echo $infos_trajet['nbrTCreated']?></span>  Trajets ajoutés</p>
          <p class="title is-4"> <span class="tag is-medium is-dark"><?php echo $infos_trajet['nbrTReserved']?></span> Trajets effectués</p>

          <p class="title is-4">Préferences</p>
          <div class="columns">
            <div class="column is-4">
              <span class="icon is-medium">
                <img src="<?=base_url("assets/img/smoking.svg");?>" aria-hidden="true">
              </span>
              <?php if ($user_preference['cigarette'] == "true"): ?>
                <span class="icon is-small" style="padding-top: 14px; padding-left: 18px; padding-left: 15px; padding-left: 13px; color:#00d1b2;">
                  <i class="fa fa-check" aria-hidden="true"></i>
                </span>
              <?php elseif($user_preference['cigarette'] == "false"): ?>
                <span class="icon is-small" style="padding-top: 14px; padding-left: 18px; padding-left: 15px; padding-left: 13px;  color:#ff3860;">
                  <i class="fa fa-times" aria-hidden="true"></i>
                </span>
              <?php else: ?>
                <span class="icon is-small" style="padding-top: 14px; padding-left: 18px; padding-left: 15px; padding-left: 13px;  color:#A4A4A4;">
                  <i class="fa fa-minus" aria-hidden="true"></i>
                </span>
              <?php endif; ?>
            </div>
            <div class="column is-4">
              <span class="icon is-medium">
                <i class="fa fa-paw" aria-hidden="true"></i>
              </span>
              <?php if ($user_preference['pets'] == "true"): ?>
                <span class="icon is-small" style="padding-top: 14px; padding-left: 18px; padding-left: 15px; padding-left: 13px; color:#00d1b2;">
                  <i class="fa fa-check" aria-hidden="true"></i>
                </span>
              <?php elseif($user_preference['pets'] == "false"): ?>
                <span class="icon is-small" style="padding-top: 14px; padding-left: 18px; padding-left: 15px; padding-left: 13px;  color:#ff3860;">
                  <i class="fa fa-times" aria-hidden="true"></i>
                </span>
              <?php else: ?>
                <span class="icon is-small" style="padding-top: 14px; padding-left: 18px; padding-left: 15px; padding-left: 13px;  color:#A4A4A4;">
                  <i class="fa fa-minus" aria-hidden="true"></i>
                </span>
              <?php endif; ?>
            </div>
            <div class="column is-4">
              <span class="icon is-medium">
                <i class="fa fa-music" aria-hidden="true"></i>
              </span>
              <?php if ($user_preference['music'] == "true"): ?>
                <span class="icon is-small" style="padding-top: 14px; padding-left: 18px; padding-left: 15px; padding-left: 13px; color:#00d1b2;">
                  <i class="fa fa-check" aria-hidden="true"></i>
                </span>
              <?php elseif($user_preference['music'] == "false"): ?>
                <span class="icon is-small" style="padding-top: 14px; padding-left: 18px; padding-left: 15px; padding-left: 13px;  color:#ff3860;">
                  <i class="fa fa-times" aria-hidden="true"></i>
                </span>
              <?php else: ?>
                <span class="icon is-small" style="padding-top: 14px; padding-left: 18px; padding-left: 15px; padding-left: 13px;  color:#A4A4A4;">
                  <i class="fa fa-minus" aria-hidden="true"></i>
                </span>
              <?php endif; ?>
            </div>
          </div>
          <p class="title is-4">Dernière connexion</p>
          <p class="subtitle is-6">le <?php echo $user['last_seen']?></p>
        </div>
      </div>
      <div class="tile is-parent">
        <div class="tile is-child notification">
          <p class="title">Biographie</p>
          <?php if ($user['bio'] == "" or $user['bio'] == null): ?>
            <p>Je n'ai pas encore rédigé ma biographie.....</p>
          <?php else:?>
            <p><?php echo $user['bio']?></p>
          <?php endif;?>
        </div>
      </div>
    </div>

  </div>
  <div id="contenu_profil_avis" style="display: none;">
    <div class="notification" id="avis_recu">
      <div class="columns">
        <div class="column">
          <p class="field is-pulled-right">
            <?php if ($user['id'] == $logged_user):?>
              <a class="button is-dark" disabled>
                <span>Ajouter un avis</span>
                <span class="icon is-small">
                  <i class="fa fa-plus"></i>
                </span>
              </a>
            <?php else:?>
              <a class="button is-dark" onclick="addReview();">
                <span>Ajouter un avis</span>
                <span class="icon is-small">
                  <i class="fa fa-plus"></i>
                </span>
              </a>
            <?php endif;?>
          </p>
          <div id="add_review" class="modal">
            <div class="modal-background"></div>
            <div class="modal-card">
              <header class="modal-card-head">
                <p class="modal-card-title">Evaluation de <?php echo $user['first_name'].' '.$user['last_name'] ?> </p>
              </header>
              <input type="hidden" value ="<?php echo $user['id']?>" name="id_user_profil" id="id_user_profil">
              <div id="content_form_review">

              </div>
            </div>
          </div>
        </div>
      </div>
      <div id="avis_ajax"></div>
    </div>
  </div>
  <div id="contenu_profil_trajets" style="display: none;">
        <div class="container">
          <?php foreach ($mes_trajets as $trajet): ?>
            <div class="card" style="width:95%;">
              <header class="card-header">
                <p class="card-header-title">
                  <?php echo date_to_str($trajet['departure']); ?>
                  <span class="tag is-light">
                    <?php echo recurrence_to_str($trajet['recurrence']); ?>
                  </span>
                </p>
                <a class="card-header-icon">
                  <span class="icon collapsed" contentid="<?php echo $trajet['trajet_id'] ?>" onclick="resizeCard(this)">
                    <i class="fa fa-angle-down"></i>
                  </span>
                </a>
              </header>
              <div class="card-content <?php echo $trajet['trajet_id'] ?>" style="display: none;">
                <div class="content">
                  <nav class="level">
                    <div class="level-item has-text-centered">
                      <div class="notification">
                        <strong>Départ : </strong><?php echo $trajet['start'] ?>
                        <br>
                        à <?php echo substr($trajet['departure'], 11, 5) ?>
                      </div>
                    </div>
                    <div class="level-item has-text-centered">
                      <div class="notification">
                        <strong>Arrivée : </strong><?php echo $trajet['destination'] ?>
                        <br>
                        à <?php echo substr($trajet['eta'], 11, 5) ?>
                      </div>
                    </div>
                  </nav>
                  <div class="box">
                    <center>
                      <span class="tag is-info"><?php echo $trajet['free_slots'] ?> places disponibles</span>
                      <span class="tag is-info"><?php echo $trajet['km'] ?> km environ</span>
                      <span class="tag is-info">~<?php echo $trajet['ept'] ?> € </span>
                    </center>
                  </div>
                  <small>Ajouté <?php echo $trajet['added_date'] ?> par vous</small>
                </div>
              </div>
              <footer class="card-footer <?php echo $trajet['trajet_id'] ?>" style="display: none;">

                <a class="card-footer-item" href="<?php echo base_url('trajets/trajet/'.$trajet['trajet_id']);?>">
                  <span class="icon">
                    <i class="fa fa-info-circle"></i>
                  </span>
                  <span>Détails  du trajet</span>
                </a>
              </footer>
            </div>
            <br><br>
          <?php endforeach; ?>
        </div>
  </div>
  </div>
  <div id="contenu_profil_contact" style="display: none;">A venir</div>

  </div>
