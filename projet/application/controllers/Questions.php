<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Questions extends CI_Controller {
	function __construct(){
    parent::__construct();
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->helper('url');

		$this->load->model('Question');
		$this->load->model('User');
		if (!is_logged_in())
			redirect('auth/login', 'refresh');
  }

	public function get_questions_view($id){
		$logged_user = $this->session->userdata('logged_in');
		$data['questions_trajet'] = $this->Question->getTrajetQuestions($id);
		$this->load->view('question_r/trajet', $data);
	}

  public function get_add_form_view(){
    $this->load->view('question_r/add_form');
  }

	public function post_question(){
		$this->load->model('User');
		$this->load->model('Question');

    $logged_user = $this->session->userdata('logged_in');
		$user_data = $this->User->getUser($logged_user['id']);

    $this->form_validation->set_rules('ques_text', 'textarea', 'required|xss_clean');

		if($this->form_validation->run() == FALSE) {
      $data = array(
        'id_return' => 0,
        'msg_form' => '<div class="notification is-danger">Erreur : veuillez rédiger votre question.</div><br>'
      );
      echo json_encode($data);
      return;
		}
		$trajet_id = $this->input->post('trajet_id');
		$ques_text = $this->input->post('ques_text');

		$question_data = array(
    	"qa_text"=> $ques_text,
			"qa_timestamp" => date('Y-m-d H:i:s'),
			"trajet_id"=> $trajet_id,
    	'user_id'=> $logged_user['id']
    );

    $this->Question->setQuestion($question_data);
    $url = base_url('trajets/trajet/'.$trajet_id);
    $data = array(
      'id_return' => 1,
      'msg_form' => '<div class="notification is-success"><div class="title">Question ajouté avec succès.</div>',
      'but_form' => '<center><a class="button is-dark" onclick="hide_modal_question()")?>Fermer</a></center></div>'
    );
    echo json_encode($data);
  }

}
