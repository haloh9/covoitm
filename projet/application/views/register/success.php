<section class="section">
  <div class="container">
    <div class="heading">
      <h1 class="title">Inscription réussie</h1>
    </div>
  </div>
</section>
<div class="content">
  <div class="columns">
    <div class="column is-1"></div>
    <div class="column auto">
      <div class="notification is-success">
          L'inscription à été effectuée avec succès,<br>
          un lien de vérification vous a été envoyé à l'adresse <strong>'<?php echo $email; ?>'</strong><br>
          <br><br>
          <center><a class="button is-medium" href="<?php echo base_url('auth')?>">Se connecter</a></center>

      </div>
      <br>
    </div>
    <div class="column is-1"></div>
  </div>
</div>
