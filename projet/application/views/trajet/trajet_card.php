<!-- Our JS -->
<script src="<?php echo base_url('assets/js/trajets/trajet_card.js') ?>" type="text/javascript"></script>
<!-- This one is for the date dropdowns -->
<script type="text/javascript" src="<?php echo base_url('assets/js/trajets/create.js') ?>"></script>
<script type="text/javascript">
  function showModalEdit(trajet_id){
    request_url = '<?=base_url("trajets/edit_modal/")?>';
    request_url += trajet_id+"/";
    request_url += '<?=urlencode(base64_encode($_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']));?>';
    $.ajax({
      type: 'GET',
      data: {
        format: 'json'
      },
      url: request_url,
      success: function(data){
        $('.modal-card-body').html(data);
        updateContent('p', 'departure-edit');
        populateDropdowns();
      }
    });
    $('#editModal').addClass("is-active");
  }

  function hideModalEdit(){
    $('#editModal').removeClass("is-active");
  }
</script>
<div class="card">
  <header class="card-header">
    <p class="card-header-title">
      <?php echo date_to_str($trajet['departure']); ?>
      <span class="tag is-light">
        <?php echo recurrence_to_str($trajet['recurrence']); ?>
      </span>
    </p>
    <a class="card-header-icon">
      <span class="icon collapsed" contentid="<?php echo $trajet['trajet_id'] ?>" onclick="resizeCard(this)">
        <i class="fa fa-angle-down"></i>
      </span>
    </a>
  </header>
  <div class="card-content <?php echo $trajet['trajet_id'] ?>" style="display: none;">
    <div class="content">
      <nav class="level">
        <div class="level-item has-text-centered">
          <div class="notification">
            <strong>Départ : </strong><?=$trajet['start']; ?>
            <br>
            à <?php echo substr($trajet['departure'], 11, 5) ?>
          </div>
        </div>
        <div class="level-item has-text-centered">
          <div class="notification">
            <strong>Arrivée : </strong><?=$trajet['destination']; ?>
            <br>
            à <?php echo substr($trajet['eta'], 11, 5) ?>
          </div>
        </div>
      </nav>
      <div class="box">
        <center>
          <span class="tag is-info"><?=$trajet['free_slots'];?> places disponibles</span>
          <span class="tag is-info"><?=$trajet['km'];?> km environ</span>
          <span class="tag is-info">~<?=$trajet['ept'];?> € </span>
        </center>
      </div>
      <?php if (strcmp($logged_user['user_id'], $creator['user_id']) == 0):?>
        <small>Ajouté <?=$trajet['added_date'];?> par vous</small>
      <?php else:?>
        <small>Ajouté <?=$trajet['added_date'];?> par <?=$creator['user_first_name'].' '.$creator['user_last_name'];?></small>
      <?php endif;?>
    </div>
  </div>
  <footer class="card-footer <?php echo $trajet['trajet_id'] ?>" style="display: none;">
    <?php if (strcmp($logged_user['user_id'], $creator['user_id']) == 0):?>
      <a class="card-footer-item" onclick='showModalEdit(<?=$trajet['trajet_id']?>)'>
        <span class="icon">
          <i class="fa fa-pencil"></i>
        </span>
        <span>Modifier</span>
      </a>
      <!--https://goo.gl/Fb1ALr URL encoding-->
      <a class="card-footer-item" href="<?php echo base_url('trajets/delete/'.$trajet['trajet_id'].'/'.urlencode(base64_encode($_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'])));?>">
        <span class="icon">
          <i class="fa fa-trash"></i>
        </span>
        <span>Supprimer</span>
      </a>
    <?php endif;?>
    <a class="card-footer-item" href="<?php echo base_url('trajets/trajet/'.$trajet['trajet_id']);?>">
      <span class="icon">
        <i class="fa fa-info-circle"></i>
      </span>
      <span>Détails</span>
    </a>
  </footer>
</div>
<!-- Modal shows up when user clicks edit-->
<div id="editModal" class="modal">
  <div class="modal-background"></div>
  <div class="modal-card">
    <header class="modal-card-head">
      <p class="modal-card-title">Modifier un trajet</p>
      <button class="delete" onclick="hideModalEdit()"></button>
    </header>
    <section class="modal-card-body">
      <center>
        <a class="button is-white is-loading">Loading</a>
      </center>
    </section>
    <footer class="modal-card-foot">
      <center>
        <button type="submit" form="editForm" class="button is-success">Sauvegarder les changements</button>
      </center>
    </footer>
  </div>
</div>
<br>
