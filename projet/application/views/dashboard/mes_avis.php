<script type="text/javascript" src="<?php echo base_url() ?>assets/js/dashboard/mes_avis.js"></script>

<div class="column is-7">
  <div class="notification is-light">
      <h1 class="title">
        Mes évaluations
      </h1>
      <div class="columns">
        <div class="column is-4"></div>
        <div class="column is-5">
          <div class="tabs is-toggle">
            <ul>
              <li id="tab_avis_recu" class="is-active" onclick="changeTab('r')">
                <a>
                  <span class="icon is-small"><i class="fa fa-image"></i></span>
                  <span>Avis reçus</span>
                </a>
              </li>
              <li  id="tab_avis_donne" onclick="changeTab('d')">
                <a>
                  <span class="icon is-small"><i class="fa fa-image"></i></span>
                  <span>Avis donnés</span>
                </a>
              </li>
            </ul>
          </div>
        </div>
        <div class="column is-3"></div>
      </div>

      <?php if ($reviews_received): ?>
        <div class="box  avis_recu">
            <?php foreach ($reviews_received as $review): ?>
              <article class="media">
                <figure class="media-left">
                  <p class="image is-64x64">
                    <img class="review_img" src="<?php echo $review['avatar'] ?>">
                  </p>
                </figure>
                <div class="media-content ">
                  <div class="content">
                    <p>
                      <strong><?php echo $review['poster'] ?></strong>
                      <br>
                      <?php echo $review['review_text'] ?>
                    </p>
                  </div>
                  <nav class="level is-mobile">
                    <small>Ajouté le : <?php echo $review['review_created_at'] ?> </small>
                  </nav>
                </div>
                <div class="media-right">
                  <?php for ($i=0; $i<$review['review_stars']; $i++): ?>
                    <span class="icon">
                      <i class="fa fa-star" style="color:#00d1b2"></i>
                    </span>
                  <?php endfor;?>
                  <?php for ($i=0; $i<(5-$review['review_stars']); $i++): ?>
                    <span class="icon">
                      <i class="fa fa-star-o" style="color:#00d1b2"></i>
                    </span>
                  <?php endfor;?>
                </div>
              </article>
            <?php endforeach; ?>
        </div>
      <?php else: ?>
        <div class="box  avis_recu">
          Vous n'avez reçu aucun avis
        </div>
      <?php endif; ?>

      <?php if ($reviews_posted): ?>
        <div class="box avis_donne" style="display: none;">
          <?php foreach ($reviews_posted as $review): ?>
              <article class="media">
                <figure class="media-left">
                  <p class="image is-64x64">
                    <img class="review_img" src="<?php echo $review['avatar'] ?>">
                  </p>
                </figure>
                <div class="media-content ">
                  <div class="content">
                    <p>
                      <strong><?php echo $review['poster'] ?></strong>
                      <br>
                      <?php echo $review['review_text'] ?>
                    </p>
                  </div>
                  <nav class="level is-mobile">
                    <small>Ajouté le : <?php echo $review['review_created_at'] ?> </small>
                  </nav>
                </div>
                <div class="media-right">
                  <?php for ($i=0; $i<$review['review_stars']; $i++): ?>
                    <span class="icon">
                      <i class="fa fa-star"></i>
                    </span>
                  <?php endfor;?>
                  <?php for ($i=0; $i<(5-$review['review_stars']); $i++): ?>
                    <span class="icon">
                      <i class="fa fa-star-o"></i>
                    </span>
                  <?php endfor;?>
                </div>
              </article>
            <?php endforeach; ?>
        </div>
      <?php else: ?>
        <div class="box avis_donne" style="display: none;">
          Vous n'avez donné aucun avis
        </div>
      <?php endif; ?>
  </div>
</div>
</div>
