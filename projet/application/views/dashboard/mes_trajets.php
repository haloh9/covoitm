




<!-- Obsolète, vérifier avant de supprimer -->














<!-- Our JS -->
<script src="<?php echo base_url('assets/js/trajets/trajet_card.js') ?>" type="text/javascript"></script>

<div class="column is-7">
  <article class="message is-info">
    <div class="message-header">
      <p>Mes trajets</p><!--<?php echo var_dump($this->_ci_cached_vars); ?>-->
      <a href="<?php echo base_url('trajets/create')?>">
        <span class="icon">
          <i class="fa fa-plus-square-o"></i>
        </span>
      </a>
    </div>
    <div class="message-body">
      <div class="container">
            <?php foreach ($mes_trajets as $trajet): ?>
              <div class="card">
                <header class="card-header">
                  <p class="card-header-title">
                    <?php echo date_to_str($trajet['departure']); ?>
                    <span class="tag is-light">
                      <?php echo recurrence_to_str($trajet['recurrence']); ?>
                    </span>
                  </p>
                  <a class="card-header-icon">
                    <span class="icon collapsed" contentid="<?php echo $trajet['trajet_id'] ?>" onclick="resizeCard(this)">
                      <i class="fa fa-angle-down"></i>
                    </span>
                  </a>
                </header>
                <div class="card-content <?php echo $trajet['trajet_id'] ?>" style="display: none;">
                  <div class="content">
                    <nav class="level">
                      <div class="level-item has-text-centered">
                        <div class="notification">
                          <strong>Départ : </strong><?php echo $trajet['start'] ?>
                          <br>
                          à <?php echo substr($trajet['departure'], 11, 5) ?>
                        </div>
                      </div>
                      <div class="level-item has-text-centered">
                        <div class="notification">
                          <strong>Arrivée : </strong><?php echo $trajet['destination'] ?>
                          <br>
                          à <?php echo substr($trajet['eta'], 11, 5) ?>
                        </div>
                      </div>
                    </nav>
                    <div class="box">
                      <center>
                        <span class="tag is-info"><?php echo $trajet['free_slots'] ?> places disponibles</span>
                        <span class="tag is-info"><?php echo $trajet['km'] ?> km environ</span>
                        <span class="tag is-info">~<?php echo $trajet['ept'] ?> € </span>
                      </center>
                    </div>
                    <small>Ajouté <?php echo $trajet['added_date'] ?> par vous</small>
                  </div>
                </div>
                <footer class="card-footer <?php echo $trajet['trajet_id'] ?>" style="display: none;">
                  <a class="card-footer-item">
                    <span class="icon">
                      <i class="fa fa-info-circle"></i>
                    </span>
                    <span>Détail du trajet</span>
                  </a>
                </footer>
              </div>
              <br><br>
            <?php endforeach; ?>
      </div>
    </div>
  </article>


</div>
</div>
