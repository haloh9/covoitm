<?php
  class User extends CI_Model {
    public function __construct() {
      $this->load->database();
    }

    public function getUser($user_id) {
      $query = $this->db->get_where('Users', array('user_id' => $user_id));
      return $query->result_array()[0];
    }

    public function getIdByMail($email){
      $query = $this->db->get_where('Users', array('user_email' => $email));
      $resultat = $query->result_array()[0];
      return $resultat['user_id'];
    }

    public function verifActivationKey($user_id, $key){
      $query = $this->db->get_where('Users', array('user_id' => $user_id));
      if($query->result_array() == null) return false;

      $resultat = $query->result_array()[0];
      if(md5($resultat['user_email']) != $key) return false;
      else {
        if($resultat['user_status'] == 1)
          return false;
        $this->db->set('user_status', 1);
        $this->db->where('user_id', $user_id);
        $this->db->update('Users');
        return true;
      }
    }

    public function setUser($user_data) {
        $user_data = array(
          'user_pass' => hash('md5', $user_data['password']),
          'user_email' => $user_data['email'],
          'user_first_name' => $user_data['first_name'],
          'user_last_name' => $user_data['last_name'],
          'user_gender' => $user_data['gender'],
          'user_birth_year' => $user_data['birth_year'],
          'user_phone' => $user_data['phone'],
          'user_role' => $user_data['role'],
          'user_avatar' => 'https://gravatar.com/avatar/'. md5($user_data['email']), // avatar par defaults
          'user_bio' => '',
          'user_balance' => 0,
          'user_loyaltypoints' => 0,
          'user_created_at' => date('Y-m-d H:i:s'),
          'user_last_seen' => null,
          'user_updated_at' => null,
          'user_status' => 0,
          'preference_id' => null
        );
        $query = $this->db->insert('Users', $user_data);
      }

    public function update_profil_user($user_data, $logged_user_id) {
        $user_data = array(
          'user_email' => $user_data['email'],
          'user_first_name' => $user_data['first_name'],
          'user_last_name' => $user_data['last_name'],
          'user_gender' => $user_data['gender'],
          'user_birth_year' => $user_data['birth_year'],
          'user_phone' => $user_data['phone'],
          'user_role' => $user_data['role'],
          'user_bio' => $user_data['bio'],
          'user_updated_at' => date('Y-m-d H:i:s'),
          'user_status' => $user_data['status']

        );
        $this->db->where('user_id', $logged_user_id);
        $query = $this->db->update('Users', $user_data);
      }

    public function isDuplicate_email($email){
        $this->db->get_where('Users', array('user_email' => $email), 1);
        if($this->db->affected_rows() > 0)
          return TRUE;
        else
          return FALSE;
    }

    public function login($email, $password){
      $this -> db -> select('user_id, user_email, user_first_name, user_pass, user_status');
      $this -> db -> from('Users');
      $this -> db -> where('user_email', $email);
      $this -> db -> where('user_pass', hash('md5', $password));
      $this -> db -> limit(1);
      $query = $this -> db -> get();
      if($query -> num_rows() == 1)
        return $query->result();
      else
        return false;
    }

    public function update_last_seen($user_id){
      $this->db->set('user_last_seen', date('Y-m-d H:i:s'));
      $this->db->where('user_id', $user_id);
      $this->db->update('Users');
    }

    public function getAverageStars($user_id){
      $this->db->from('Reviews');
      $this->db->where(array('user_reviewed' => $user_id));
      $query = $this->db->get();
      $rows = $query->result_array();
      $cpt = 0;
      $nbr_rows = 0;
      if($rows){
        foreach ($rows as $row) {
          $cpt += $row['review_stars'];
          $nbr_rows++;
        }
        return $cpt/$nbr_rows;
      } else
        return 0;
    }

    public function if_email_exist($email){
      $this->db->select('user_id, user_pass, user_email');
      $this->db->from('Users');
      $this->db->where('user_email', $email);
      $this->db->limit(1);
      $query = $this->db->get();

      if($query->num_rows() == 1){
        $q = $query->result();
        return $q[0];
      } else
        return false;
    }

    public function update_password($user_id, $new_password){
      $this->db->set('user_pass', hash('md5', $new_password));
      $this->db->set('user_updated_at', date('Y-m-d H:i:s'));
      $this->db->where('user_id', $user_id);
      $this->db->update('Users');
    }

    public function update_wallet($user_id, $new_balance){
      $this->db->set('user_balance', $new_balance);
      $this->db->where('user_id', $user_id);
      $this->db->update('Users');
    }

    public function is_verified($id){
      $query = $this->db->get_where('Users', array('user_id' => $id));
      $user = $query->result_array()[0];
      if($user['user_status'] == 0)
        return false;
      else
        return true;
    }

    public function getUserTrajetCount($id) {
      $query = $this->db->get_where('Trajets', Array('user_id' => $id), 0, 20);
      return $this->db->count_all_results();
    }
}
