<?php
function is_logged_in() {
    // Get current CodeIgniter instance
    $CI =& get_instance();
    // We need to use $CI->session instead of $this->session
    $test_login = $CI->session->userdata('logged_in');
    if (isset($test_login))
      return true;
    else
      return false;
}

function is_verified() {
  if(!is_logged_in()) return true;
  $CI =& get_instance();
  $user_logged = $CI->session->userdata('logged_in');
  $user_status = $CI->User->is_verified($user_logged['id']);
  if($user_status == 0)
    return false;
  else
    return true;
}

function get_balance() {
  $CI =& get_instance();
  $user_logged = $CI->session->userdata('logged_in');
  $user_data = $CI->User->getUser($user_logged['id']);
  return $user_data['user_balance'];
}

function recurrence_to_str($recurrence) {
  switch ($recurrence) {
    case 'p':
      return 'Ponctuel';
      break;
    case 'h':
      return 'Hebdomadaire';
      break;
    case 'q':
      return 'Quotidien';
      break;
    default:
      # code...
      break;
  }
}

function date_to_str($date) {
  $unix_date = strtotime($date);
  $wday = getdate($unix_date)['weekday'];
  $day = substr($date, 8, 2);
  $month = getdate($unix_date)['month'];
  $year = substr($date, 0, 4);
  return $wday.' '.$day.' '.$month.' '.$year;
}

function generate_group($length = 5) {
  $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
  $charactersLength = strlen($characters);
  $randomString = '';
  for ($i = 0; $i < $length; $i++) {
      $randomString .= $characters[rand(0, $charactersLength - 1)];
  }
  return $randomString;
}

function days_to_array($days) {
  $array = array();
  if (isset($days['monday']))
    $array[] = 'Monday';
  if (isset($days['tuesday']))
    $array[] = 'Tuesday';
  if (isset($days['wednesday']))
    $array[] = 'Wednesday';
  if (isset($days['thursday']))
    $array[] = 'Thursday';
  if (isset($days['friday']))
    $array[] = 'Friday';
  if (isset($days['saturday']))
    $array[] = 'Saturday';
  if (isset($days['sunday']))
    $array[] = 'Sunday';
  return $array;
}

function datetime_to_time($date, $split = '', $json = false) {
  switch ($split) {
    case 'h':
      $value = substr($date, 11, 2);
      break;
    case 'm':
      $value = substr($date, 14, 2);
      break;

    default:
      $value = substr($date, 11, 5);
      break;
  }
  if ($json) return json_encode($value);
  else return $value;
}

function datetime_to_date($date, $split = '', $json = false) {
  switch ($split) {
    case 'y':
      $value = substr($date, 0, 4);
      break;
    case 'm':
      $value = substr($date, 5, 2);
      break;
    case 'd':
      $value = substr($date, 8, 2);
      break;

    default:
      $value = substr($date, 8, 2).'/'.substr($date, 5, 2).'/'.substr($date, 0, 4);
      break;
  }
  if ($json) return json_encode($value);
  else return $value;
}

function generate_dropdown_options($free_slots) {
  $options = array();
  for ($i=1; $i <= $free_slots; $i++) {
    $options[$i] = $i;
  }
  return $options;
}
