<div class="column is-7">
  <div class="notification is-warning">
    <h1 class="title">
      Mes notifications
    </h1>
    <div class="notification is-light">
      <?php if (!$new_notif && !$viewed_notif): ?>
        <p>Vous n'avez aucune notification.</p>
      <?php else: ?>
        <?php foreach ($new_notif as $notif): ?>

          <article class="media is-info">
            <div class="media-left">
              <span class="icon is-medium">
                <i class="fa fa-bell"></i>
              </span>
            </div>
            <div class="media-content ">
              <div class="content">
                <p><?php echo $notif['notification_titre'] ?></p>
              </div>
              <nav class="level is-mobile">
                <small>Ajouté le : <?php echo $notif['notification_created_at'] ?> </small>
              </nav>
            </div>
          </article>
        <?php endforeach; ?>
        <?php foreach ($viewed_notif as $notif): ?>
          <article class="media">
            <div class="media-left">
              <span class="icon is-medium">
                <i class="fa fa-bell-o"></i>
              </span>
            </div>
            <div class="media-content is-info">
              <div class="content">
                <p><?php echo $notif['notification_titre'] ?></p>
              </div>
              <nav class="level is-mobile">
                <small>Ajouté le : <?php echo $notif['notification_created_at'] ?> </small>
              </nav>
            </div>
          </article>
        <?php endforeach; ?>
      <?php endif; ?>
    </div>
  </div>
</div>
</div>
