<?php
  class Notification extends CI_Model {

    public function __construct() {
      $this->load->database();
    }

    public function getNbrOfNotif($user_id){
      $this->db->from('Notifications');
      $this->db->where(array('user_id' => $user_id, 'notification_viewed' => false));
      return $this->db->count_all_results();
    }

    public function setNotif($user_id, $titre, $action){
      $notif_data = array(
        'notification_titre' => $titre,
        'notification_action' => $action,
        'notification_viewed' => 0,
        'notification_created_at' => date('Y-m-d H:i:s'),
        'user_id' => $user_id
      );
      $query = $this->db->insert('Notifications', $notif_data);
    }

    public function getNotifs($user_id, $viewed){
      $this->db->from('Notifications');
      $this->db->where(array('user_id' => $user_id, 'notification_viewed' => $viewed));
      $this->db->order_by("notification_created_at", "DESC");
      $query = $this->db->get();
      return $query->result_array();
    }

    public function makeNotifsViewed($user_id){
      $this->db->set('notification_viewed', 1, FALSE);
      $this->db->where(array('user_id' => $user_id, 'notification_viewed' => false));
      $this->db->update('Notifications');
    }


  }
