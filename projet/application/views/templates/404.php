<section class="hero is-dark is-medium">
  <div class="hero-body ">
    <div class="container has-text-centered">
      <h1 class="title" style="font-size: 70px;color:#00d1b2">404 error</h1>
      <h2 class="subtitle is-3">Page not found</h2>

        <a class="button is-primary" href="<?php echo base_url('')?>">
          <span class="icon">
            <i class="fa fa-home"></i>
          </span>
          <span>Page d'accueil</span>
        </a>
    </div>
  </div>
</section>
