function initAutocomplete() {
  var bouchesDuRhone = new google.maps.LatLngBounds(
    new google.maps.LatLng(43.113828, 4.205364),
    new google.maps.LatLng(43.952396, 5.801689));

  var from = $('[name=from]')[0];
  var to = $('[name=to]')[0];
  var options = {
    bounds: bouchesDuRhone,
    strictBounds: true,
    types: []
  };

  autocomplete = new google.maps.places.Autocomplete(from, options);
  autocomplete = new google.maps.places.Autocomplete(to, options);
}
