<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Verify extends CI_Controller {

	function __construct(){
    parent::__construct();

  }


	public function index()
	{
    redirect('');
	}

  public function process($user_id, $key){
    $this->load->model('user');

    $this->load->view('templates/header');
    if($this->User->verifActivationKey($user_id, $key))
      $this->load->view('verification/index');
    else
      $this->load->view('verification/error');

    $this->load->view('templates/footer');
  }
}
