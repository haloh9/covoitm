<div class="column is-7">
  <div class="notification is-info">
    <h2 class="title">Mes réservations</h2>
    <?php if (count($mes_reservations) < 1): ?>
      <div class="notification is-light">
        Vous n'avez aucune réservation.
      </div>
    <?php else:?>
    <?php foreach ($mes_reservations as $trajet): ?>
      <div class="card">
        <header class="card-header">
          <p class="card-header-title">
            <?php echo substr($trajet['departure'], 0, 10) ?>
            ((Quotidien? Hebdo?))
          </p>
        </header>
        <div class="card-content">
          <div class="content">
            <nav class="level">
              <div class="level-item has-text-centered">
                <div class="notification">
                  <strong>Départ : </strong><?php echo $trajet['start'] ?>
                  <br>
                  à <?php echo substr($trajet['departure'], 11, 5) ?>
                </div>
              </div>
              <div class="level-item has-text-centered">
                <div class="notification">
                  <strong>Arrivée : </strong><?php echo $trajet['destination'] ?>
                  <br>
                  à <?php echo substr($trajet['eta'], 11, 5) ?>
                </div>
              </div>
            </nav>
            <div class="box">
              <center>
                <span class="tag is-warning"><?php echo $trajet['free_slots'] ?> places disponibles</span>
                <span class="tag is-warning"><?php echo $trajet['km'] ?> km environ</span>
                <span class="tag is-warning">~<?php echo $trajet['ept'] ?> € </span>
              </center>
            </div>
            <small>Ajouté <?php echo $trajet['added_date'] ?> par ((user))</small>
          </div>
        </div>
        <footer class="card-footer">
          <a class="card-footer-item">
            <span class="icon">
              <i class="fa fa-info-circle"></i>
            </span>
            <span>Détail du trajet</span>
          </a>
        </footer>
      </div>
      <br><br>
    <?php endforeach;?>
    <?php endif;?>
  </div>


</div>
</div>
