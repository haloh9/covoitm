<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notifications extends CI_Controller {

  function __construct() {
    parent::__construct();
    $this->load->model('Notification');

  }

  public function index() {
  }

  public function set_notif($titre){
    $user_data = $this->session->userdata('logged_in');
    $this->Notification->setNotif($user_data['id'], urldecode($titre), "/dashboard/mes_avis");
  }

  public function get_nbrof_notif(){
    $user_data = $this->session->userdata('logged_in');
    echo $this->Notification->getNbrOfNotif($user_data['id']);
  }

  public function get_new_notif(){
    $user_data = $this->session->userdata('logged_in');
    $data['new_notif'] = $this->Notification->getNotifs($user_data['id'], false);
    $this->load->view('notification/index', $data);
    $this->Notification->makeNotifsViewed($user_data['id']);
  }
}
