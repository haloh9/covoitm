function changeTab(tab){
  if(tab=='r'){
    $( ".avis_donne" ).css( "display", "none" );
    $( ".avis_recu" ).css( "display", "block" );
    $( "#tab_avis_donne" ).removeClass("is-active");
    $( "#tab_avis_recu" ).addClass("is-active");

  } else if(tab=='d'){
    $( ".avis_donne" ).css( "display", "block" );
    $( ".avis_recu" ).css( "display", "none" );
    $( "#tab_avis_recu" ).removeClass("is-active");
    $( "#tab_avis_donne" ).addClass("is-active");
  }
}
