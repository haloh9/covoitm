<script>

function step1_paye(){
  var amount_cm = $("#amount_cm").val();
  console.log(amount_cm);
  if(amount_cm == ""){
    $('#error_step1_msg').html("Veuillez saisir un montant");
  } else if (amount_cm<1 || amount_cm>20){
    $('#error_step1_msg').html("Minimum d'achat : 1€<br>Maximum d'achat : 20€");
  } else {
    $("#amount").val(amount_cm)
    var url;
    url = "crediter/success/"+amount_cm;
    $("#return_success").val("<?php echo base_url('" +url+"') ?>")

    $("#confirmation_msg").html('Veuillez confirmer votre achat de '+amount_cm+'€')
    $("#form_step1").css( "display", "none" );
    $("#form_step2").css( "display", "" );
  }
}

function back_step1(){
  $("#form_step2").css( "display", "none" );
  $("#form_step1").css( "display", "" );
}

</script>
<div class="column is-7">
  <div class="notification is-dark">
      <h1 class="title">
        CREDITER
      </h1>
      <?php echo $this->session->flashdata('error_msg_paypal'); ?>
      <div id="form_step1">
        <div class="field has-addons has-addons-centered">
          <p class="control">
            <span class="select">
              <select>
                <option>€</option>
              </select>
            </span>
          </p>
          <p class="control">
            <input id="amount_cm" class="input" type="number" placeholder="Monant d'achat">
          </p>
          <p class="control">
            <a class="button is-primary" onclick="step1_paye()">
              Suivre
            </a>
          </p>
        </div>
        <center><div class="help" id="error_step1_msg"></div></center>
      </div>

      <div id="form_step2" class="columns" style="display: none;">
        <div class="column has-text-centered	">
          <div class="notification is-info">
            <form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post">
              <input type="hidden" name="cmd" value="_xclick">
              <input type="hidden" name="business" value="soufianeben94-facilitator@gmail.com">
              <input type="hidden" name="lc" value="CA">
              <input type="hidden" name="item_name" value="Solde CovoitM">
              <input type="hidden" name="button_subtype" value="services">
              <input type="hidden" name="no_note" value="0">
              <input type="hidden" name="cn" value="Add special instructions to the seller">
              <input type="hidden" name="no_shipping" value="2">
              <input type="hidden" id="amount" name="amount" value="">
              <input type="hidden" name="currency_code" value="EUR">
              <p class="control">
                <div id="confirmation_msg" class="subtitile is-2"></div><br>
                <input class="button is-success" type="submit" value="Confirmer"/>
              </p>
              <p class="help">vous serez redirger vers la page de paiement paypal</p>
              <input type="hidden" id="return_success" name="return" value="">
              <input type="hidden" id="return_failed" name="cancel_return" value="<?php echo base_url('crediter/failed') ?>">
            </form>
          </div>
          <button class="button" onclick="back_step1()">Retour</button>
        </div>
      </div>

  </div>
</div>
</div>
