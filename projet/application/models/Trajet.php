<?php
  class Trajet extends CI_Model {
    public function __construct() {
      $this->load->database();
    }

    //TODO: edit offset and limit to make pagination
    //      limit=20 (elements/page), offset=0 (page num)
    public function getTrajets() {
      $query = $this->db->get('Trajets', 20, 0);
      return $query->result_array();
    }

    public function getTrajetByID($id) {
        $query = $this->db->get_where('Trajets', Array('trajet_id' => $id), 20, 0);
        return $query->result_array()[0];

    }

    public function getPreference($user_id) {
      $query = $this->db->get_where('Preference_set', array('user_id' => $user_id));
      return $query->result_array()[0];
    }



    public function getTrajetFromTo($from, $to, $filtres) {
      $this->db->select('*');
      $this->db->from('Trajets');
      $this->db->where(array('start' => $from, 'destination' => $to));
      $this->db->join('Preference_set', 'Preference_set.user_id = Trajets.user_id');
      $query = $this->db->get();
      $rows =  $query->result_array();
      $resultas = array();
      if($rows){
        foreach ($rows as $row) {
          if(($filtres['cigarette'] == "true") && ($row['cigarette'] == "false"))
            break;
          if(($filtres['pets'] == "true") && ($row['pets'] == "false"))
            break;
          if(($filtres['music'] == "true") && ($row['music'] == "false"))
              break;
          if($filtres['cigarette'] == "false"){
            if($row['cigarette'] == "true") break;
            if($row['cigarette'] == "neute") break;
          }
          if($filtres['pets'] == "false"){
            if($row['pets'] == "true") break;
            if($row['pets'] == "neute") break;
          }
          if($filtres['music'] == "false"){
            if($row['music'] == "true") break;
            if($row['music'] == "neute") break;
          }
          $resultas[] = $row;
        }
      }
      return $resultas;

    }

    public function getTrajetFrom($from, $filtres) {
      $this->db->select('*');
      $this->db->from('Trajets');
      $this->db->where('start', $from);
      $this->db->join('Preference_set', 'Preference_set.user_id = Trajets.user_id');
      $query = $this->db->get();
      $rows =  $query->result_array();
      $resultas = array();
      if($rows){
        foreach ($rows as $row) {
          if(($filtres['cigarette'] == "true") && ($row['cigarette'] == "false"))
            break;
          if(($filtres['pets'] == "true") && ($row['pets'] == "false"))
            break;
          if(($filtres['music'] == "true") && ($row['music'] == "false"))
              break;
          if($filtres['cigarette'] == "false"){
            if($row['cigarette'] == "true") break;
            if($row['cigarette'] == "neute") break;
          }
          if($filtres['pets'] == "false"){
            if($row['pets'] == "true") break;
            if($row['pets'] == "neute") break;
          }
          if($filtres['music'] == "false"){
            if($row['music'] == "true") break;
            if($row['music'] == "neute") break;
          }
          $resultas[] = $row;
        }
      }
      return $resultas;
    }

    public function getTrajetTo($to, $filtres) {
      $this->db->select('*');
      $this->db->from('Trajets');
      $this->db->where('destination', $to);
      $this->db->join('Preference_set', 'Preference_set.user_id = Trajets.user_id');
      $query = $this->db->get();
      $rows =  $query->result_array();
      $resultas = array();
      if($rows){
        foreach ($rows as $row) {
          if(($filtres['cigarette'] == "true") && ($row['cigarette'] == "false"))
            break;
          if(($filtres['pets'] == "true") && ($row['pets'] == "false"))
            break;
          if(($filtres['music'] == "true") && ($row['music'] == "false"))
              break;
          if($filtres['cigarette'] == "false"){
            if($row['cigarette'] == "true") break;
            if($row['cigarette'] == "neute") break;
          }
          if($filtres['pets'] == "false"){
            if($row['pets'] == "true") break;
            if($row['pets'] == "neute") break;
          }
          if($filtres['music'] == "false"){
            if($row['music'] == "true") break;
            if($row['music'] == "neute") break;
          }
          $resultas[] = $row;
        }
      }
      return $resultas;

    }

    public function setTrajet($trajet_info){
    }

    public function getTrajetsCreatedBy($user_id) {
      $query = $this->db->get_where('Trajets', array('user_id' => $user_id), 20, 0);
      return $query->result_array();
    }
    public function getNumberOfTrajetsCreatedBy($user_id) {
      $this->db->from('Trajets');
      $this->db->where(array('user_id' => $user_id));
      return $this->db->count_all_results();
    }
    public function getNumberOfTrajetsReservedBy($user_id) {
      $this->db->from('reserve');
      $this->db->where(array('user_id' => $user_id));
      return $this->db->count_all_results();

    }
    
    public function getTrajetsReservedBy($user_id) {
      $query = $this->db->get_where('reserve', array('user_id' => $user_id), 20, 0);
      $reservations = $query->result_array();
      foreach ($reservations as $res) {
        $query2 = $this->db->get_where('Trajets', array('trajet_id' => $res['trajet_id']), 20, 0);
        $row = $query2->result_array();
        $trajets[] = $row[0]; // [] means append to array
      }
      if (isset($trajets))
        return $trajets;
      else {
          return array();
      }
    }

    public function deleteTrajet($trajet_id) {
      return $this->db->delete('Trajets', array('trajet_id' => $trajet_id));
    }

    public function update($trajet_id, $trajet_data) {
      $this->db->update('Trajets', $trajet_data, array('trajet_id' => $trajet_id));
    }
  }
