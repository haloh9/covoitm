#------------------------------------------------------------
#        Script MySQL.
#------------------------------------------------------------


#------------------------------------------------------------
# Table: Users
#------------------------------------------------------------

CREATE TABLE Users(
        user_id            int (11) Auto_increment  NOT NULL ,
        user_pass          Char (64) NOT NULL ,
        user_email         Char (50) NOT NULL ,
        user_first_name    Char (50) NOT NULL ,
        user_last_name     Char (50) NOT NULL ,
        user_gender        Bool NOT NULL ,
        user_birth_year    Int NOT NULL ,
        user_phone         Varchar (25) NOT NULL ,
        user_role          Int NOT NULL ,
        user_avatar        Varchar (512) NOT NULL ,
        user_bio           Text ,
        user_balance       Decimal (10,2) ,
        user_loyaltypoints Int ,
        user_created_at    Datetime ,
        user_updated_at    Datetime ,
        user_last_seen     Datetime ,
        user_status        Bool NOT NULL ,
        preference_id      Int NULL ,
        PRIMARY KEY (user_id ) ,
        UNIQUE (user_email )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Trajets
#------------------------------------------------------------

CREATE TABLE Trajets(
        trajet_id   int (11) Auto_increment  NOT NULL ,
        departure   Datetime NOT NULL ,
        start       Char (45) NOT NULL ,
        destination Char (45) NOT NULL ,
        eta         Datetime ,
        free_slots  Int NOT NULL ,
        recurrence  Char (25) ,
        km          Decimal (10,2) NOT NULL ,
        ept         Int ,
        grouping    Varchar (25) ,
        added_date  Date NOT NULL ,
        user_id     Int NOT NULL ,
        PRIMARY KEY (trajet_id ) ,
        INDEX (start ,destination )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Reviews
#------------------------------------------------------------

CREATE TABLE Reviews(
        review_id         int (11) Auto_increment  NOT NULL ,
        review_text       Text NOT NULL ,
        review_stars      Bool ,
        review_created_at Datetime NOT NULL ,
        user_reviewed     Int ,
        user_id           Int NOT NULL ,
        PRIMARY KEY (review_id )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Q_A
#------------------------------------------------------------

CREATE TABLE Q_A(
        qa_id        int (11) Auto_increment  NOT NULL ,
        qa_timestamp Datetime NOT NULL ,
        qa_text      Text NOT NULL ,
        trajet_id    Int NOT NULL ,
        user_id      Int NOT NULL ,
        PRIMARY KEY (qa_id )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Preference_set
#------------------------------------------------------------

CREATE TABLE Preference_set(
        preference_id int (11) Auto_increment  NOT NULL ,
        cigarette     Varchar (25) NOT NULL ,
        pets          Varchar (25) NOT NULL ,
        music         Varchar (25) NOT NULL ,
        user_id       Int NOT NULL ,
        PRIMARY KEY (preference_id )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Notifications
#------------------------------------------------------------

CREATE TABLE Notifications(
        notification_id         int (11) Auto_increment  NOT NULL ,
        notification_titre      Varchar (150) NOT NULL ,
        notification_action     Varchar (50) ,
        notification_viewed     Bool NOT NULL ,
        notification_created_at Datetime ,
        user_id                 Int NOT NULL ,
        PRIMARY KEY (notification_id )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: reserve
#------------------------------------------------------------

CREATE TABLE reserve(
        reserve_date  Datetime NOT NULL ,
        reserve_slots Int NOT NULL ,
        user_id       Int NOT NULL ,
        trajet_id     Int NOT NULL ,
        PRIMARY KEY (user_id ,trajet_id )
)ENGINE=InnoDB;

ALTER TABLE Users ADD CONSTRAINT FK_Users_preference_id FOREIGN KEY (preference_id) REFERENCES Preference_set(preference_id) ON DELETE SET NULL;
ALTER TABLE Trajets ADD CONSTRAINT FK_Trajets_user_id FOREIGN KEY (user_id) REFERENCES Users(user_id) ON DELETE CASCADE;
ALTER TABLE Reviews ADD CONSTRAINT FK_Reviews_user_id FOREIGN KEY (user_id) REFERENCES Users(user_id) ON DELETE CASCADE;
ALTER TABLE Q_A ADD CONSTRAINT FK_Q_A_trajet_id FOREIGN KEY (trajet_id) REFERENCES Trajets(trajet_id) ON DELETE CASCADE;
ALTER TABLE Q_A ADD CONSTRAINT FK_Q_A_user_id FOREIGN KEY (user_id) REFERENCES Users(user_id) ON DELETE CASCADE;
ALTER TABLE Preference_set ADD CONSTRAINT FK_Preference_set_user_id FOREIGN KEY (user_id) REFERENCES Users(user_id) ON DELETE CASCADE;
ALTER TABLE Notifications ADD CONSTRAINT FK_Notifications_user_id FOREIGN KEY (user_id) REFERENCES Users(user_id) ON DELETE CASCADE;
ALTER TABLE reserve ADD CONSTRAINT FK_reserve_user_id FOREIGN KEY (user_id) REFERENCES Users(user_id) ON DELETE SET NULL;
ALTER TABLE reserve ADD CONSTRAINT FK_reserve_trajet_id FOREIGN KEY (trajet_id) REFERENCES Trajets(trajet_id) ON DELETE SET NULL;
