<!-- Our JS -->
<script src="<?php echo base_url('assets/js/trajets/create.js') ?>" type="text/javascript"></script>

<section class="section">
  <div class="container">
    <div class="heading">
      <h1 class="title">Nouveau trajet</h1>
      <h2 class="subtitle"></h2>
    </div>
  </div>
</section>
<div class="columns search-bcg">
  <div class="column is-3"></div>
  <div class="column is-6">
    <div class="content">
      <div class="box">
        <?php echo form_open('trajets/process'); ?>
          <h2 class="subtitle">Veuillez saisir les informations relatives au trajet
            <br>
            <span class="help is-danger"><?php echo validation_errors();
            echo $this->session->flashdata('flash_message'); ?></span>
          </h2>
          <div class="field is-horizontal">
            <div class="field-label is-normal">
              <label class="label">Départ</label>
            </div>
            <div class="field-body">
              <div class="field is-grouped">
                <p class="control is-expanded has-icon has-icon-left">
                  <input class="input is-primary is-medium" type="text" name="start" placeholder="Villette...">
                  <span class="icon is-medium">
                    <i class="fa fa-user"></i>
                  </span>
                </p>
              </div>
            </div>
          </div>

          <div class="field is-horizontal">
            <div class="field-label is-normal">
              <label class="label">Destination</label>
            </div>
            <div class="field-body">
              <div class="field is-grouped">
                <p class="control is-expanded has-icon has-icon-left">
                  <input class="input is-primary is-medium" type="text" name="destination" placeholder="Saint-Charles...">
                  <span class="icon is-medium">
                    <i class="fa fa-user"></i>
                  </span>
                </p>
              </div>
            </div>
          </div>

          <div class="field is-horizontal">
            <div class="field-label is-normal">
              <label class="label">Places disponibles</label>
            </div>
            <div class="field-body">
              <div class="field is-grouped">
                <p class="control is-expanded has-icon has-icon-left">
                  <input class="input is-primary is-medium" type="number" min="1" max="9" name="free_slots" placeholder="4" style="width: 30%;">
                  <span class="icon is-medium">
                    <i class="fa fa-user"></i>
                  </span>
                </p>
              </div>
            </div>
          </div>
          <div class="control">
            <div class="tabs is-centered">
              <ul>
                <li id="q" onclick="changeTab('q')"><a>Quotidien</a></li>
                <li id="h" onclick="changeTab('h')"><a>Hebdomadaire</a></li>
                <!--<li id="m" onclick="changeTab('m')"><a>Mensuel</a></li>-->
                <li id="p" onclick="changeTab('p')" class="is-active"><a>Ponctuel</a></li>
              </ul>
            </div>
          </div>
          <div id="tab-content">
          </div>
          <input id="recurrence" name="recurrence" type="text" hidden>
          <p class="control">
            <center>
              <button type="submit" class="button is-success is-large">Créer</button>
            </center>
          </p>
        </form>
      </div>
    </div>
  </div>
  <div class="column is-3"></div>
</div>
<script>
function initAutocomplete() {
  var bouchesDuRhone = new google.maps.LatLngBounds(
    new google.maps.LatLng(43.113828, 4.205364),
    new google.maps.LatLng(43.952396, 5.801689));

  var start = $('[name=start]')[0];
  var destination = $('[name=destination]')[0];
  var options = {
    bounds: bouchesDuRhone,
    strictBounds: true,
    types: []
  };

  autocomplete = new google.maps.places.Autocomplete(start, options);
  autocomplete = new google.maps.places.Autocomplete(destination, options);
}
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDUb9C0NnsLI7DWPHFj8QmKnEK6VXIHZjg&libraries=places&callback=initAutocomplete" async defer></script>
